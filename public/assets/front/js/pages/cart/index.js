function updateFirstTotal() {
    $("#firstTotal").html('<i class="icon-spinner icon-spin"></i>')
    $.get('/api/cart/firstTotal')
        .done((data) => {
            $("#firstTotal").html(`<strong>${data.total}</strong>`)
        })
        .fail(() => {
            toastr.error("Impossible de mettre à jour le total")
        })
}

function updateTotal() {
    $("#fulltotal").html('<i class="icon-spinner icon-spin"></i>')
    $.get('/api/cart/getTotal')
        .done((data) => {
            $("#fulltotal").html(`<strong>${data.total}</strong>`)
        })
        .fail(() => {
            toastr.error("Impossible de mettre à jour le total")
        })
}






(function ($) {
    updateFirstTotal()
    updateTotal()
    $(".remove").on('click', function (e) {
        e.preventDefault();
        let btn = $(this);
        let url = btn.attr('href');

        btn.html(`<i class="icon-spinner icon-spin"></i>`)

        $.ajax({
            url: url,
            method: 'DELETE',
            success: function (data) {
                if (data.code === 200) {
                    toastr.success("Le produit à bien été supprimer de votre panier");
                    updateFirstTotal()
                    updateTotal()
                    btn.parents('tr').fadeOut();
                }
            },
            error: function () {
                toastr.error("Erreur lors de la suppression du produit");
            }
        })
    })
})(jQuery)
