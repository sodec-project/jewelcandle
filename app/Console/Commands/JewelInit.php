<?php

namespace App\Console\Commands;

use App\HelperClass\CarrierClass;
use app\HelperClass\CartClass;
use App\Imports\ProductCategoryImport;
use App\Imports\ProductCombinationImport;
use App\Imports\ProductImport;
use App\Imports\ProductSubCategoryImport;
use App\Model\Improve\Carrier\Carrier;
use app\Packages\Sendcloud\Method;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class JewelInit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jewel:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialisation de la base de donnée de jewel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bar = $this->output->createProgressBar(7);
        if ($this->confirm("Le service Jewel va être initialiser, voulez-vous continuer ?")) {
            $this->line("Initialisation ...");
            $bar->start();
            $this->initDatabase();
            $bar->advance();
            $this->installCategory();
            $bar->advance();
            $this->installSubcategories();
            $bar->advance();
            $this->installProduct();
            $bar->advance();
            $this->installProductCombination();
            $bar->advance();
            $this->installCarrier();
            $bar->advance();
            $this->installPaymentMethod();
            $bar->advance();
            $this->info("Initialisation de Jewel Terminer");
            $bar->finish();
            return null;
        } else {
            $this->error("Initialisation annulée");
            return null;
        }
    }

    public function initDatabase()
    {
        $this->line("Réinitialisation de la base de donnée");
        $this->call('migrate:fresh');
        $this->info("Réinitialisation de la base de donnée: TERMINER");
    }


    private function installCategory()
    {
        $this->line("Installation des catégories");
        if (Excel::import(new ProductCategoryImport(), storage_path().'/app/public/import/product_categories.csv', null, \Maatwebsite\Excel\Excel::CSV)) {
            $this->info("Installation des catégories: Terminer");
        }else{
            $this->error("Erreur lors de l'installation des catégories");
        }
    }

    private function installSubcategories()
    {
        $this->line("Installation des sous catégories");
        if(Excel::import(new ProductSubCategoryImport(), storage_path().'/app/public/import/product_sub_categories.csv', null, \Maatwebsite\Excel\Excel::CSV)) {
            $this->info("Installation des sous catégories: TERMINER");
        } else {
            $this->error("Erreur lors de l'installation des sous catégories");
        }
    }

    private function installProduct()
    {
        $this->line("Installation des produits");
        if(Excel::import(new ProductImport(), storage_path().'/app/public/import/product.csv', null, \Maatwebsite\Excel\Excel::CSV)) {
            $this->info("Installation des Produits: TERMINER");
        } else {
            $this->error("Erreur lors de l'installation des produits.");
        }
    }

    private function installProductCombination()
    {
        $this->line("Installation des combinaison de produit");
        try {
            $this->call('db:seed', [
                "--class" => "ProductCombinationSeeder"
            ]);
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
        $this->info("Installation des combinaison de produit: TERMINER");
    }

    private function installCarrier()
    {
        $this->line("Installation des transporteurs");
        try {
            $this->call('db:seed', [
                "--class" => "CarrierSeeder"
            ]);
        }catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
        $this->info("Installation des transporteurs: TERMINER");

    }

    private function installPaymentMethod()
    {
        $this->line("Installation des modes de Paiements");
        try{
            $this->call('db:seed', [
                "--class"   => "PaymentMethodSeeder"
            ]);
            $this->info("Installation des modes de Paiements: TERMINER");
        }catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }
    }


}
