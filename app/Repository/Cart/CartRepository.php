<?php
namespace App\Repository\Cart;

use App\Model\Cart\Cart;

class CartRepository
{
    /**
     * @var Cart
     */
    private $cart;

    /**
     * CartRepository constructor.
     * @param Cart $cart
     */

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function isExits()
    {
        return $this->cart->newQuery()
            ->where('cart_token', session()->token())
            ->count();
    }

    public function initCart($_token)
    {
        return $this->cart->newQuery()
            ->create([
                "cart_token"    => $_token,
                "ipAddress"     => request()->ip()
            ]);
    }

    public function getCart()
    {
        return $this->cart->newQuery()->where('cart_token', session()->get('_token'))->first()->load('cartproducts');
    }

    public function updateTotalCart($id, $updateTotalCart)
    {
        return $this->cart->newQuery()
            ->find($id)
            ->update(["total" => $updateTotalCart]);
    }

}

