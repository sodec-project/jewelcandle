<?php

namespace App\Repository\Cart;

use App\Model\Cart\CartProduct;

class CartProductRepository
{
    /**
     * @var CartProduct
     */
    private $cartProduct;

    /**
     * CartProductRepository constructor.
     * @param CartProduct $cartProduct
     */

    public function __construct(CartProduct $cartProduct)
    {
        $this->cartProduct = $cartProduct;
    }

    public function exist($cart_id, $product_id, $combi_id)
    {
        $count = $this->cartProduct->newQuery()
            ->where('cart_id', $cart_id)
            ->where('product_id', $product_id)
            ->where('combination_id', $combi_id)
            ->count();

        if ($count == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function get($cart_id, $product_id, $combi_id)
    {
        return $this->cartProduct->newQuery()
            ->where('cart_id', $cart_id)
            ->where('product_id', $product_id)
            ->where('combination_id', $combi_id)
            ->first()
            ->load('cart', 'product', 'combination');
    }

    public function update($id, $updateQuantity, $updateTotalPrice)
    {
        return $this->cartProduct->newQuery()
            ->find($id)
            ->update([
                "qte"   => $updateQuantity,
                "totalPrice"    => $updateTotalPrice
            ]);
    }

    public function updateTotalCart($cart_id)
    {
        $products = $this->cartProduct->newQuery()
            ->where('cart_id', $cart_id)
            ->sum('totalPrice');

        return $products;
    }

    public function create($id, $product_id, $combi_id, $price, int $qte, $totalPrice)
    {
        return $this->cartProduct->newQuery()
            ->create([
                "cart_id"       => $id,
                "product_id"    => $product_id,
                "combination_id"=> $combi_id,
                "unitPrice"     => $price,
                "qte"           => $qte,
                "totalPrice"    => $totalPrice
            ]);
    }

    public function getProduct($cartProduct_id)
    {
        return $this->cartProduct->newQuery()
            ->find($cartProduct_id);
    }

    public function delete($cartProduct_id)
    {
        try {
            return $this->cartProduct->newQuery()
                ->find($cartProduct_id)
                ->delete();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}

