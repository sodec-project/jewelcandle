<?php
namespace App\Repository\Order;

use App\Model\Order\OrderProduct;

class OrderProductRepository
{
    /**
     * @var OrderProduct
     */
    private $orderProduct;

    /**
     * OrderProductRepository constructor.
     * @param OrderProduct $orderProduct
     */

    public function __construct(OrderProduct $orderProduct)
    {
        $this->orderProduct = $orderProduct;
    }

    public function create($id, $product_id, $combination_id, $unitPrice, $qte, $total_price)
    {
        return $this->orderProduct->newQuery()
            ->create([
                "order_id"      => $id,
                "product_id"    => $product_id,
                "combination_id"=> $combination_id,
                "UnitPrice"     => $unitPrice,
                "quantity"      => $qte,
                "totalPrice"    => $total_price
            ]);
    }

}

