<?php
namespace App\Repository\Order;

use App\Model\Order\Order;

class OrderRepository
{
    /**
     * @var Order
     */
    private $order;

    /**
     * OrderRepository constructor.
     * @param Order $order
     */

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function create($id, string $string, $total)
    {
        return $this->order->newQuery()->create([
            "user_id"   => $id,
            "numOrder"  => $string,
            "totalOrder"=> $total
        ]);
    }

    public function get($id)
    {
        return $this->order->newQuery()
            ->find($id)
            ->load('products', 'payments', 'deliveries', 'returns');
    }


}

