<?php
namespace App\Repository\Product;

use App\Model\Product\ProductImage;

class ProductImageRepository
{
    /**
     * @var ProductImage
     */
    private $productImage;

    /**
     * ProductImageRepository constructor.
     * @param ProductImage $productImage
     */

    public function __construct(ProductImage $productImage)
    {
        $this->productImage = $productImage;
    }

}

