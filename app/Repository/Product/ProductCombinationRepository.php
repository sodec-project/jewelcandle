<?php

namespace App\Repository\Product;

use App\Model\Product\ProductCombination;

class ProductCombinationRepository
{
    /**
     * @var ProductCombination
     */
    private $productCombination;

    /**
     * ProductCombinationRepository constructor.
     * @param ProductCombination $productCombination
     */

    public function __construct(ProductCombination $productCombination)
    {
        $this->productCombination = $productCombination;
    }

    public function updateStockToInventory($product_id, $size_id)
    {

    }

    public function getCombination($item)
    {
        return $this->productCombination->newQuery()
            ->find($item)->load('product');
    }

}

