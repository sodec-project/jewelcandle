<?php

namespace App\Repository\Product;

use App\Model\Product\ProductCategorie;

class ProductCategoryRepository
{
    /**
     * @var ProductCategorie
     */
    private $productCategorie;

    /**
     * ProductCategoryRepository constructor.
     * @param ProductCategorie $productCategorie
     */

    public function __construct(ProductCategorie $productCategorie)
    {
        $this->productCategorie = $productCategorie;
    }

    public function all()
    {
        return $this->productCategorie->newQuery()
            ->get();
    }

    public function list()
    {
        return $this->all()->load('subcategories');
    }

    public function create($category_name)
    {
        return $this->productCategorie->newQuery()
            ->create([
                "name" => $category_name
            ]);
    }

    public function getForName($category_name)
    {
        return $this->productCategorie->newQuery()
            ->where('name', "LIKE", '%' . $category_name . '%')
            ->first();
    }

    public function update($id, $category_name)
    {
        $this->productCategorie->newQuery()
            ->find($id)
            ->update([
                "name"  => $category_name
            ]);

        return $this->get($id);
    }

    public function get($id)
    {
        return $this->productCategorie->newQuery()
            ->find($id);
    }

    public function getForSlug($slug_category)
    {
        return $this->productCategorie->newQuery()
            ->where('slug', $slug_category)
            ->first()
            ->load('subcategories');
    }


}

