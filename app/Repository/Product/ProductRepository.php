<?php
namespace App\Repository\Product;

use App\Model\Product\Product;

class ProductRepository
{
    /**
     * @var Product
     */
    private $product;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function latestProduct()
    {
        return $this->product->newQuery()
            ->orderBy('created_at', 'asc')
            ->limit(4)
            ->get()
            ->load('images');
    }

    public function getProduct($product_id)
    {
        return $this->product->newQuery()
            ->find($product_id)
            ->load('categorie', 'subcategorie', 'combinations', 'images', 'votes');
    }

    public function list()
    {
        return $this->product->newQuery()
            ->get()
            ->load('categorie', 'subcategorie', 'combinations', 'images', 'votes');
    }

    public function getForCategory($id)
    {
        return $this->product->newQuery()
            ->where('product_categorie_id', $id)
            ->get()
            ->load('categorie', 'subcategorie', 'combinations', 'images', 'votes');
    }

    public function countProduct()
    {
        return $this->product->newQuery()
            ->count();
    }


}

