<?php
namespace App\Repository\Product;

use App\Model\Product\ProductShipping;

class ProductShippingRepository
{
    /**
     * @var ProductShipping
     */
    private $productShipping;

    /**
     * ProductShippingRepository constructor.
     * @param ProductShipping $productShipping
     */

    public function __construct(ProductShipping $productShipping)
    {
        $this->productShipping = $productShipping;
    }

}

