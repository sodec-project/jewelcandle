<?php

namespace App\Repository\Product;

use App\Model\Product\ProductSubCategory;

class ProductSubCategoryRepository
{
    /**
     * @var ProductSubCategory
     */
    private $productSubCategory;

    /**
     * ProductSubCategoryRepository constructor.
     * @param ProductSubCategory $productSubCategory
     */

    public function __construct(ProductSubCategory $productSubCategory)
    {
        $this->productSubCategory = $productSubCategory;
    }

    public function create($id, $category_name)
    {
        return $this->productSubCategory->newQuery()
            ->create([
                "product_categorie_id" => $id,
                "name" => $category_name
            ]);
    }

    public function getForName($category_name)
    {
        return $this->productSubCategory->newQuery()
            ->where('name', 'LIKE', '%' . $category_name . '%')
            ->first();
    }

    public function update($id, $categorie_id, $category_name)
    {
        $this->productSubCategory->newQuery()
            ->find($id)
            ->update([
                "product_categorie_id" => $categorie_id,
                "name" => $category_name
            ]);
    }

    public static function getCountForCategory($category_id)
    {
        return ProductSubCategory::query()->where('product_categorie_id', $category_id)->count();
    }

}

