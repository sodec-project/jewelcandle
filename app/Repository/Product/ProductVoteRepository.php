<?php
namespace App\Repository\Product;

use App\Model\Product\ProductVote;

class ProductVoteRepository
{
    /**
     * @var ProductVote
     */
    private $productVote;

    /**
     * ProductVoteRepository constructor.
     * @param ProductVote $productVote
     */

    public function __construct(ProductVote $productVote)
    {
        $this->productVote = $productVote;
    }

    public function hallOfFame()
    {
        return $this->productVote->newQuery()
            ->orderBy('solde', 'asc')
            ->limit(4)
            ->get()
            ->load('product');
    }

}

