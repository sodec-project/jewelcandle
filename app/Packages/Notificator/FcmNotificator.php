<?php


namespace app\Packages\Notificator;


use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Notification;
use paragraph1\phpFCM\Recipient\Topic;

class FcmNotificator extends Config
{
    public function notificator($title, $text, $topic = "weather")
    {
        $message = new Message();
        $message->addRecipient(new Topic($topic))
            ->setNotification(new Notification($title, $text));

        $this->client->send($message);
    }
}
