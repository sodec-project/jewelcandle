<?php


namespace app\Packages\Notificator;


use paragraph1\phpFCM\Client;

class Config
{
    /**
     * @var Client
     */
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
        $this->client->setApiKey(env("FCM_SECRET_KEY"));
        $this->client->injectHttpClient(new \GuzzleHttp\Client());
    }

}
