<?php


namespace app\Packages\Hiboutik;


use Illuminate\Support\Arr;

class Product extends Config
{
    public function listProducts()
    {
        try {
            $products = $this->shop->get('products');
            return $products;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getProduct($product_id)
    {
        try {
            $product = $this->shop->get('products/'.$product_id);
            return $product[0];
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getStocks($product_id)
    {
        try {
            $stocks = $this->shop->get('stock_available/product_id/'.$product_id);
            return $stocks;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function getPriceForProductSize($product_id, $size_id){
        $product = $this->getProduct($product_id);
        //dd($product);

        $query = Arr::get($product, 'product_specific_rules');
        dd($query);
    }
}
