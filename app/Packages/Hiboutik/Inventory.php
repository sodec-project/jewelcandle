<?php


namespace app\Packages\Hiboutik;


use Illuminate\Support\Arr;

class Inventory extends Config
{
    public function listInventory()
    {
        try {
            $inventories = $this->shop->get('inventory_inputs');
            return $inventories;
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function lastInventory()
    {
        try {
            $inventories = $this->listInventory();
            $ar = Arr::first($inventories);
            return $ar;
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    public function listStockToInventory($inventory_id)
    {
        try {
            $stocks = $this->shop->get('/inventory_input_details/'.$inventory_id);
            return $stocks;
        }catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}
