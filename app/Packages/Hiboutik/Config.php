<?php


namespace app\Packages\Hiboutik;


use Hiboutik\HiboutikAPI;
use http\Client\Request;

class Config
{
    /**
     * @var string
     */
    private $endpoint = "https://jewelcandle.hiboutik.com/api/";
    private $account;
    private $user;
    private $key;
    protected $shop;

    public function __construct()
    {
        $this->account = env("HIBOUTIK_ACCOUNT");
        $this->user = env("HIBOUTIK_EMAIL");
        $this->key = env("HIBOUTIK_KEY");

        $this->shop = new HiboutikAPI($this->account, $this->user, $this->key);
    }

    public function onlineCheck()
    {
        $timeout = 10;
        $curlInit = curl_init('https://jewelcandle.hiboutik.com/docapi/json/');
        curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,$timeout);
        curl_setopt($curlInit,CURLOPT_HEADER,true);
        curl_setopt($curlInit,CURLOPT_NOBODY,true);
        curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);
        $reponse = curl_exec($curlInit);
        curl_close($curlInit);
        if ($reponse) return true;
        return false;
    }
}
