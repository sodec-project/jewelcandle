<?php


namespace app\Packages\Hiboutik;


class Categories extends Config
{
    /**
     * Liste des Catégories
     * @return array|string
     */
    public function listCategories()
    {
        try {
            $categories = $this->shop->get('categories');
            return $categories;
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }


}
