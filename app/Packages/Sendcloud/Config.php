<?php


namespace app\Packages\Sendcloud;


use Picqer\Carriers\SendCloud\Connection;
use Picqer\Carriers\SendCloud\SendCloud;

class Config
{
    /**
     * @var SendCloud
     */
    protected $sendcloud;

    public function __construct()
    {
        $connection = new Connection(env("SENDCLOUD_PUB"),env("SENDCLOUD_SECRET"));
        $this->sendcloud = new SendCloud($connection);
    }
}
