<?php


namespace app\Packages\Sendcloud;


class Method extends Config
{
    public function all()
    {
        return $this->sendcloud->shippingMethods()->all();
    }

    public function getMethod($method_id)
    {
        return $this->sendcloud->shippingMethods()->find($method_id);
    }
}
