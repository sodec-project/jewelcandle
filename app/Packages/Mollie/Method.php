<?php


namespace app\Packages\Mollie;


use Mollie\Api\Exceptions\ApiException;

class Method extends Config
{
    /**
     * Liste des modes de paiements actif
     * @return \Mollie\Api\Resources\BaseCollection|\Mollie\Api\Resources\MethodCollection|string retourne la liste des paiements actif
     */
    public function listMethod()
    {
        try {
            $methods = $this->mollie->methods->allActive();
            return $methods;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    public function listAllMethod()
    {
        try {
            $methods = $this->mollie->methods->allAvailable();
            return $methods;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    public function getMethod($method_id)
    {
        try {
            $method = $this->mollie->methods->get($method_id);
            return $method;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

}
