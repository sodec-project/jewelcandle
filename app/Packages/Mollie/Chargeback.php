<?php


namespace app\Packages\Mollie;


use Mollie\Api\Exceptions\ApiException;

class Chargeback extends Config
{
    /**
     * Liste de tous les contentieux
     * @return \Mollie\Api\Resources\ChargebackCollection|string
     */
    public function listAllChargeback()
    {
        try {
            $charges = $this->mollie->chargebacks->page();
            return $charges;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Liste de tous les contentieux par paiement
     * @param $payment_id
     * @return \Mollie\Api\Resources\ChargebackCollection|string
     */
    public function listChargebackFromPayment($payment_id)
    {
        try {
            $charges = $this->mollie->payments->get($payment_id)->chargebacks();
            return $charges;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Information sur un contentieux
     * @param $payment_id
     * @param $charge_id
     * @return \Mollie\Api\Resources\Chargeback|string
     */
    public function getChargeback($payment_id, $charge_id)
    {
        try {
            $charge = $this->mollie->payments->get($payment_id)->getChargeback($charge_id);
            return $charge;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }
}
