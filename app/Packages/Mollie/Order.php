<?php


namespace app\Packages\Mollie;


use Mollie\Api\Exceptions\ApiException;

class Order extends Config
{
    public function createOrder($order_id, $montant, $givenName, $familyName, $email, $organizationName = null, $streetAndNumber = null, $city = null, $region = null, $postalCode = null, $country = null, $title = null, $phone = null, $streetAdditional = null, $method = null)
    {
        $amount = [
            "currency"  => "EUR",
            "value"     => $montant
        ];

        $billingAdress = [
            "organizationName" => $organizationName,
            "streetAndNumber" => $streetAndNumber,
            "city" => $city,
            "region" => $region,
            "postalCode" => $postalCode,
            "country" => $country,
            "title" => $title,
            "givenName" => $givenName,
            "familyName" => $familyName,
            "email" => $email,
            "phone" => $phone,
        ];

        $shippingAddress = [
            "organizationName" => $organizationName,
            "streetAndNumber" => $streetAndNumber,
            "streetAdditional" => $streetAdditional,
            "city" => $city,
            "region" => $region,
            "postalCode" => $postalCode,
            "country" => $country,
            "title" => $title,
            "givenName" => $givenName,
            "familyName" => $familyName,
            "email" => $email,
        ];

        $metadata = [
            "order_id" => $order_id
        ];

        $lines = [
            [
                "type"  => "physical",
                "name"  => "Règlement de la commande N°".$order_id,
                "quantity"  => 1,
                "unitPrice" => [
                    "currency"  => "EUR",
                    "value"     => $montant
                ],
                "totalAmount" => [
                    "currency"  => "EUR",
                    "value"     => $montant
                ],
                "vatRate"   => "0.00",
                "vatAmount" => [
                    "currency"  => "EUR",
                    "value"     => "0.00"
                ],
            ]
        ];

        try {
            $order = $this->mollie->orders->create([
                "amount" => $amount,
                "billingAddress" => $billingAdress,
                "shippingAddress" => $shippingAddress,
                "locale" => 'fr_FR',
                "orderNumber" => $order_id,
                "redirectUrl" => route('home'),
                "method" => $method,
                "metadata" => $metadata,
                "lines" => $lines
            ]);

            return $order;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    public function getOrder($order_id)
    {
        try {
            $order = $this->mollie->orders->get($order_id);
            return $order;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }


}
