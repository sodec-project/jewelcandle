<?php


namespace app\Packages\Mollie;


use Mollie\Api\Exceptions\ApiException;
use Mollie\Api\MollieApiClient;

class Config
{
    private $env;
    private $key;
    protected $mollie;

    public function __construct()
    {
        // Déclaration de l'environnement
        $this->env = env("MOLLIE_ENV");
        if ($this->env == 'test') {
            $this->key = env("MOLLIE_TEST_KEY");
        } else {
            $this->key = env("MOLLIE_PROD_KEY");
        }

        // Création de l'instance
        $this->mollie = new MollieApiClient();
        try {
            $this->mollie->setApiKey($this->key);
        } catch (ApiException $e) {
            return $e->getMessage();
        }

    }
}
