<?php


namespace app\Packages\Mollie;


use Mollie\Api\Exceptions\ApiException;
use Mollie\Api\Resources\PaymentCollection;
use Mollie\Laravel\Facades\Mollie;

class Payment extends Config
{
    /**
     * Création d'un paiement
     * @param string $value Montant à charger
     * @param string $numOrder
     * @return string|null URL de redirection
     */
    public function createPayment($value, $numOrder)
    {
        try {
            $payment = $this->mollie->payments->create([
                "amount" => [
                    "currency" => "EUR",
                    "value" => $value
                ],
                "description" => "JEWELCANDLE CMD N°" . $numOrder,
                "redirectUrl" => route('home'),
                "metadata" => [
                    "order_id" => $numOrder
                ]
            ]);
            $payment = $this->mollie->payments->get($payment->id);
            return $payment->getCheckoutUrl();
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Get Payment
     * @param integer $id Identifiant du paiement
     * @return \Mollie\Api\Resources\Payment|string Retourne les information du paiement
     */
    public function getPayment($id)
    {
        try {
            $payment = $this->mollie->payments->get($id);
            return $payment;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Update Payment
     * @param integer $id
     * @param string $numOrder
     * @return \Mollie\Api\Resources\Payment|string retourne l'instance du paiement mis à jour
     */
    public function updatePayment($id, $numOrder)
    {
        try {
            $payment = $this->mollie->payments->get($id);

            $payment->description = "JEWELCANDLE CMD N°" . $numOrder;
            $payment->redirectUrl = route('home');
            $payment->metadata = ["order_id" => $numOrder];

            $payment->update();

            return $this->mollie->payments->get($id);
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * Annule un paiement
     * @param integer $id
     * @return \Mollie\Api\Resources\Payment|string retourne une instance du paiement
     */
    public function cancelPayment($id)
    {
        try {
            $payment = $this->mollie->payments->cancel($id);
            return $payment;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * List Paiements
     * @return PaymentCollection|string retourne la liste des paiements
     */
    public function listPayment()
    {
        try {
            $payments = $this->mollie->payments->page();
            return $payments;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }


}
