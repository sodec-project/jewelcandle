<?php


namespace app\Packages\Mollie;


use Mollie\Api\Exceptions\ApiException;

class Refunds extends Config
{
    /**
     * Création d'un remboursement
     * @param $payment_id
     * @param null $value
     * @param bool $complete
     * @return \Mollie\Api\Resources\BaseResource|string
     */
    public function createRefund($payment_id, $value = null, $complete = true)
    {
        try {
            $payment = $this->mollie->payments->get($payment_id);
            if ($complete == true) {
                $refund = $payment->refund([
                    "amount"    => [
                        "currency"  => "EUR",
                        "value"     => $payment->amount->value
                    ]
                ]);
            } else {
                $refund = $payment->refund([
                    "amount"    => [
                        "currency"  => "EUR",
                        "value"     => $value
                    ]
                ]);
            }
            return $refund;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Information sur un remboursement
     * @param $payment_id
     * @param $refund_id
     * @return \Mollie\Api\Resources\Refund|string
     */
    public function getRefund($payment_id, $refund_id)
    {
        try {
            $refund = $this->mollie->payments->get($payment_id)->getRefund($refund_id);
            return $refund;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Annulation d'un remboursement
     * @param $payment_id
     * @param $refund_id
     * @return string|null
     */
    public function cancelRefund($payment_id, $refund_id)
    {
        try {
            $this->getRefund($payment_id, $refund_id)->cancel();
            return null;
        } catch (ApiException $e) {
            return $e->getMessage();
        }

    }

    /**
     * Liste des remboursement par paiement
     * @param $payment_id
     * @return \Mollie\Api\Resources\RefundCollection|string
     */
    public function listRefundsForPayment($payment_id)
    {
        try {
            $refunds = $this->mollie->payments->get($payment_id)->refunds();
            return $refunds;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Liste de tous les remboursements
     * @return \Mollie\Api\Resources\RefundCollection|string
     */
    public function listAllRefunds()
    {
        try {
            $refunds = $this->mollie->refunds->page();
            return $refunds;
        } catch (ApiException $e) {
            return $e->getMessage();
        }
    }
}
