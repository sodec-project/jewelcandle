<?php


namespace app\Packages\Sendingblue;


use GuzzleHttp\Client;
use SendinBlue\Client\Api\ListsApi;
use Vansteen\Sendinblue\Facades\Sendinblue;

class Config
{
    private $config;
    /**
     * @var ListsApi
     */
    public $instance;

    public function __construct()
    {
        $this->config = Sendinblue::getConfiguration();
        $this->instance = new ListsApi(new Client(), $this->config);
    }
}
