<?php

namespace App\Imports;

use App\Model\Product\ProductCategorie;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductCategoryImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProductCategorie([
            "name"  => $row['name'],
            "slug"  => Str::slug($row['name'])
        ]);
    }
}
