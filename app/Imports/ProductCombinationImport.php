<?php

namespace App\Imports;

use App\Model\Product\ProductCombination;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductCombinationImport implements ToModel, WithHeadingRow, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProductCombination([
            "product_id"    => $row['product_id'],
            "name"          => $row['name'],
            "price"         => $row['price'],
            "quantity"      => $row['quantity'],
            "default"       => $row['default']
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getCsvSettings(): array
    {
        // TODO: Implement getCsvSettings() method.
        return [
            "delimiter" => ";"
        ];
    }
}
