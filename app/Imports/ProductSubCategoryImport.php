<?php

namespace App\Imports;

use App\Model\Product\ProductSubCategory;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductSubCategoryImport implements ToModel, WithHeadingRow, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ProductSubCategory([
            "product_categorie_id"  => $row['product_categorie_id'],
            "name"                  => $row['name'],
            "slug"                  => Str::slug($row['name'])
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getCsvSettings(): array
    {
        // TODO: Implement getCsvSettings() method.
        return [
            "delimiter" => ";"
        ];
    }
}
