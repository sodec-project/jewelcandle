<?php

namespace App\Imports;

use App\Model\Product\Product;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ProductImport implements ToModel, WithHeadingRow, WithCustomCsvSettings
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            "product_categorie_id"  => $row['product_categorie_id'],
            "product_sub_categorie_id" => $row['product_sub_categorie_id'],
            "name"  => $row['name'],
            "reference" => $row['reference'],
            "price"     => $row['price'],
            "summary"   => $row['summary'],
            "description" => $row['description'],
            "combination" => $row['combination'],
            "quantity"      => $row['quantity'],
            "weight"        => $row['weight'],
            "slug"      => Str::slug($row['name'])
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getCsvSettings(): array
    {
        // TODO: Implement getCsvSettings() method.
        return [
            "delimiter" => ";"
        ];
    }
}
