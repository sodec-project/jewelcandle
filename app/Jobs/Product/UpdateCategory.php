<?php

namespace App\Jobs\Product;

use App\Model\Product\ProductCategorie;
use App\Notifications\Admin\Product\FailedUpdateCategoryNotification;
use App\Notifications\Admin\Product\UpdateCategoryNotification;
use App\Notifications\Fcm\Product\UpdateCategoryFcmNotification;
use app\Packages\Hiboutik\Categories;
use app\Packages\Notificator\FcmNotificator;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateCategory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var \Illuminate\Database\Eloquent\Model|object|static|null
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = User::query()->where('email', 'contact@jewelcandle.shop')->first();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $hiboutik = new Categories();

        foreach ($hiboutik->listCategories() as $listCategory) {
            $categorieIsExist = ProductCategorie::query()->where('name', 'LIKE', '%'.$listCategory['category_name'])->count();
            //dd($categorieIsExist);
            if($listCategory['category_id_parent'] == 0) {
                if($categorieIsExist == 0) {
                    ProductCategorie::query()->create([
                        "name"  => $listCategory['category_name'],
                        "hiboutik_id"   => $listCategory['category_id']
                    ]);
                } else {
                    $categorie = ProductCategorie::query()->where('name', 'LIKE', '%'.$listCategory['category_name'])->first();
                    ProductCategorie::query()->find($categorie->id)->update([
                        "name"  => $listCategory['category_name'],
                        "hiboutik_id"   => $listCategory['category_id']
                    ]);
                }
            }
        }

        $this->user->notify(new UpdateCategoryNotification());
        (new FcmNotificator())->notificator(env("APP_NAME"), "Catégorie Mise à jour");
    }

    public function failed(\Exception $exception)
    {
        $this->user->notify(new FailedUpdateCategoryNotification($exception));
        (new FcmNotificator())->notificator(env("APP_NAME"), "Erreur lors de la mise à jours des catégories");
    }
}
