<?php

namespace App\Jobs\Product;

use App\Model\Product\ProductCategorie;
use App\Model\Product\ProductSubCategory;
use App\Notifications\Admin\Product\FailedUpdateCategoryNotification;
use App\Notifications\Admin\Product\FailedUpdateSubCategoryNotification;
use App\Notifications\Admin\Product\UpdateSubCategoryNotification;
use app\Packages\Hiboutik\Categories;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateSubCategory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var \Illuminate\Database\Eloquent\Model|object|static|null
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = User::query()->where('email', 'contact@jewelcandle.shop')->first();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $hiboutik = new Categories();
        foreach ($hiboutik->listCategories() as $listCategory) {
            $subcategoryIsExist = ProductSubCategory::query()->where('hiboutik_id', $listCategory['category_id'])->count();
            //dd($subcategoryIsExist);
            if ($subcategoryIsExist == 0) {
                $categoryIsExist = ProductCategorie::query()->where('hiboutik_id', $listCategory['category_id_parent'])->count();
                if ($categoryIsExist != 0) {
                    $category = ProductCategorie::query()->where('hiboutik_id', $listCategory['category_id_parent'])->first();
                    ProductSubCategory::query()->create([
                        "product_categorie_id" => $category->id,
                        "name" => $listCategory['category_name'],
                        "hiboutik_id" => $listCategory['category_id']
                    ]);
                }
            }
        }

        $this->user->notify(new UpdateSubCategoryNotification());
    }

    public function failed(\Exception $exception)
    {
        $this->user->notify(new FailedUpdateSubCategoryNotification($exception));
    }
}
