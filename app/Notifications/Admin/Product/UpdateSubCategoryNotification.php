<?php

namespace App\Notifications\Admin\Product;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class UpdateSubCategoryNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', WebPushChannel::class, 'slack', 'fcm'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
            ->subject("Tache: Update SubCategory")
            ->greeting('Tache Effectuer')
            ->line("Le Job 'Update SubCategory' à été effectuer avec succès");
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title('Tache effectuer')
            ->icon('/storage/icons/success.png')
            ->data("La tache <strong>Update SubCategory</strong> à été éxécuter avec succès");
    }

    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->to('#jewelcandleshop')
            ->success()
            ->content('La tache "Update SubCategory" à été executer avec succès');
    }

    public function toFcm($notifiable)
    {
        $message = new FcmMessage();
        $message->setHeaders([
            'project_id'    =>  env("FCM_SENDER_ID")   // FCM sender_id
        ])->content([
            'title'        => env("APP_NAME"),
            'body'         => "Les sous catégories ont été mise à jours",
            'icon'         => '/storage/icons/success.png', // Optional
        ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
