<?php

namespace App\Notifications\Admin\Product;

use Benwilkins\FCM\FcmMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Minishlink\WebPush\WebPush;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;
use Snowfire\Beautymail\Beautymail;

class FailedUpdateCategoryNotification extends Notification
{
    use Queueable;
    private $exception;

    /**
     * Create a new notification instance.
     *
     * @param $exception
     */
    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [WebPushChannel::class, 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->error()
            ->subject("Tache: Update Catégorie")
            ->greeting("Tache en erreur")
            ->line("La Tache 'Update Category' est en erreur !")
            ->line($this->exception);
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title('Failed Job')
            ->icon('/storage/icons/warning.png')
            ->data("Une erreur à eu lieu lors de l'execution d'un JOB: UpdateCategory");
    }

    public function toSlack($notifiable)
    {
        $exception = $this->exception;
        return (new SlackMessage)
            ->to('#jewelcandleshop')
            ->error()
            ->content('Erreur')
            ->attachment(function ($attachment) use ($exception) {
                $attachment->title('Exception: ', $exception)
                    ->content("Erreur d'execution du JOB");
            });
    }


    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
