<?php

namespace App\Notifications\Admin\Product;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class UpdateCategoryNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [WebPushChannel::class, 'slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
            ->subject("Tache: Update Catégorie")
            ->greeting('Tache Effectuer')
            ->line("Le Job 'Update Category' à été effectuer avec succès");
    }

    public function toWebPush($notifiable, $notification)
    {
        return (new WebPushMessage)
            ->title('Tache effectuer')
            ->icon('/storage/icons/success.png')
            ->data("La tache <strong>Update Category</strong> à été éxécuter avec succès");
    }

    public function toSlack($notifiable)
    {
        return (new SlackMessage)
            ->to('#jewelcandleshop')
            ->success()
            ->content('La tache "Update Category" à été executer avec succès');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
