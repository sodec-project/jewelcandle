<?php
if(!function_exists('verifImageExist')){
    function verifImageExist($directory){
        $data = \Illuminate\Support\Facades\Storage::disk('public')->exists($directory);
        return $data;

    }
}
