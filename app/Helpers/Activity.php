<?php
if(!function_exists('addingActivity')){
    function addingActivity(string $designation, string $icons, int $state){
        $activity = new \App\Model\User\UserActivity();

        $activity->newQuery()->insert([
            "user_id"   => auth()->user()->id,
            "activity"  => $designation,
            "icons"     => $icons,
            "state"     => $state,
            "created_at"=> now(),
            "updated_at"=> now()
        ]);
    }
}
