<?php


namespace app\HelperClass;


use App\Model\Product\Product;
use App\Model\Product\ProductImage;
use App\Model\Product\ProductVote;

class ProductClass
{
    public static function getCountVote($product_id)
    {
        $product = ProductVote::query()->where('product_id', $product_id);
        $sumVote = $product->sum('vote');
        $countVote = $product->count('vote');

        if($sumVote != 0 && $countVote != 0) {
            $calc = $sumVote / $countVote;
            if ($calc >= 0 && $calc <= 0.50) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star-half-full"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 0.50 && $calc <= 1) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 1.01 && $calc <= 1.50) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star-half-full"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 1.51 && $calc <= 2) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 2.01 && $calc <= 2.50) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star-half-full"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 2.51 && $calc <= 3) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 3.01 && $calc <= 3.50) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star-half-full"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 3.51 && $calc <= 4) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
            } elseif ($calc >= 4.01 && $calc <= 4.50) {
                return '
             <div class="product-rating" data-toggle="tooltip" title="'.$calc.'/5">
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star-half-full"></i>
             </div>
            ';
            } else {
                return '
             <div class="product-rating">
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
                 <i class="icon-star3"></i>
             </div>
            ';
            }
        }else{
            return '
             <div class="product-rating">
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
                 <i class="icon-star-empty"></i>
             </div>
            ';
        }
    }

    public static function productheaderForCategory($category_id)
    {
        return Product::query()
            ->where('product_categorie_id', $category_id)
            ->orderBy('id', 'asc')
            ->limit(3)
            ->get();
    }

    public static function latestProduct()
    {
        return Product::query()
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();
    }

    public static function iconCombination($value)
    {
        switch ($value)
        {
            case 'Bague': return "<i class='icon2-diamond-ring'></i>"; break;
            case 'Collier': return "<i class='icon2-necklace'></i>"; break;
            case 'Charm': return "<i class='icon2-heart-pendant'></i>"; break;
            case 'Boucles d\'oreilles': return "<i class='icon2-jewelry'></i>"; break;
            case 'Bracelet': return "<i class='icon2-bracelet'></i>"; break;
        }
    }
}
