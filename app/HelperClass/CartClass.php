<?php


namespace app\HelperClass;


use App\Model\Cart\Cart;
use App\Model\Cart\CartProduct;

class CartClass
{
    private static function init()
    {
        return Cart::query()
            ->where('cart_token', session()->get('_token'))
            ->first();
    }

    public static function initCount()
    {
        return Cart::query()
            ->where('cart_token', session()->get('_token'))
            ->count();
    }

    public static function getMyCart()
    {
        return self::init()->load('cartproducts');
    }

    public static function countProductToCart()
    {
        $cart = self::init();
        return CartProduct::query()->where('cart_id', $cart->id)->count();
    }

    public static function sumProductToCart()
    {
        $cart = self::init();
        return CartProduct::query()->where('cart_id', $cart->id)->sum('totalPrice');
    }
}
