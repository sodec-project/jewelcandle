<?php


namespace app\HelperClass;


use App\Model\Product\ProductCategorie;
use App\Model\Product\ProductSubCategory;

class CategoryClass
{
    public static function listCategory()
    {
        return ProductCategorie::query()
            ->orderBy('id', 'desc')
            ->get()
            ->load('subcategories');
    }

    public static function countSub($category_id)
    {
        return ProductSubCategory::query()
            ->where('product_categorie_id', $category_id)
            ->get()
            ->count();
    }
}
