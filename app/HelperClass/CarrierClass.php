<?php


namespace App\HelperClass;


use App\Model\Cart\Cart;
use App\Model\Improve\Carrier\Carrier;
use app\Packages\Sendcloud\Method;
use Illuminate\Support\Arr;

class CarrierClass
{
    public static function estimatePriceOfShipping($cart_id)
    {
        $cart = new Cart();
        $carrier = new Carrier();

        $query = $cart->newQuery()->find($cart_id)->load('cartproducts');

        $array = [];

        foreach ($query->cartProducts as $data) {
            $array[] = $data->product->weight;
        }

        $weight = array_sum($array);

        $colissimos = $carrier->newQuery()
            ->where('type', 1)
            ->get();

        foreach ($colissimos as $colissimo) {
            if($weight >= $colissimo->min_weight && $weight <= $colissimo->max_weight) {
                return $colissimo->price;
            } else {
                return $colissimo;
            }
        }
    }
}
