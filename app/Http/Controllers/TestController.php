<?php

namespace App\Http\Controllers;

use App\HelperClass\CarrierClass;
use App\Model\Improve\Carrier\Carrier;
use app\Packages\Sendcloud\Method;
use Illuminate\Support\Arr;
use function Psy\debug;

class TestController extends Controller
{
    public function test()
    {
        dd(CarrierClass::estimatePriceOfShipping(1));
    }
}
