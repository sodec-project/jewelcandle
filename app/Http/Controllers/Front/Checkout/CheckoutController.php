<?php

namespace App\Http\Controllers\Front\Checkout;

use App\Http\Controllers\Controller;
use App\Repository\Cart\CartRepository;
use App\Repository\Order\OrderProductRepository;
use App\Repository\Order\OrderRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var OrderProductRepository
     */
    private $orderProductRepository;

    /**
     * CheckoutController constructor.
     * @param CartRepository $cartRepository
     * @param OrderRepository $orderRepository
     * @param OrderProductRepository $orderProductRepository
     */
    public function __construct(CartRepository $cartRepository, OrderRepository $orderRepository, OrderProductRepository $orderProductRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->orderRepository = $orderRepository;
        $this->orderProductRepository = $orderProductRepository;
    }

    public function index()
    {
        if(auth()->check() == false) {
            return redirect()->route('login');
        }else{
            return view("front.checkout.index", [
                "cart"  => $this->cartRepository->getCart()
            ]);
        }
    }

    public function shipping()
    {
        $cart = $this->cartRepository->getCart();
        try {
            $order = $this->orderRepository->create(auth()->user()->id, "ORD".Carbon::now()->format('Y.m.d').generateNum(4), $cart->total);

            try {
                foreach ($cart->cartproducts as $product) {
                    $this->orderProductRepository->create($order->id, $product->product_id, $product->combination_id, $product->unitPrice, $product->qte, $product->total_price);
                    return view('front.checkout.shipping', [
                        "order" => $this->orderRepository->get($order->id)
                    ]);
                }
            } catch (\Exception $exception) {
                connectify('error', "Passage de la commande", $exception->getMessage());
                return redirect()->back();
            }

        }catch (\Exception $exception) {
            connectify('error', "Passage de la commande", $exception->getMessage());
            return redirect()->back();
        }
    }
}
