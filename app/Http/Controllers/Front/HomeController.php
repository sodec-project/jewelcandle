<?php

namespace App\Http\Controllers\Front;

use app\HelperClass\CategoryClass;
use App\Http\Controllers\Controller;
use App\Mail\Core\ContactMail;
use App\Repository\Cart\CartRepository;
use App\Repository\Product\ProductRepository;
use App\Repository\Product\ProductVoteRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductVoteRepository
     */
    private $productVoteRepository;
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * HomeController constructor.
     * @param ProductRepository $productRepository
     * @param ProductVoteRepository $productVoteRepository
     * @param CartRepository $cartRepository
     */
    public function __construct(ProductRepository $productRepository, ProductVoteRepository $productVoteRepository, CartRepository $cartRepository)
    {
        $this->productRepository = $productRepository;
        $this->productVoteRepository = $productVoteRepository;

        $this->cartRepository = $cartRepository;
    }

    public function index()
    {
        //dd(CategoryClass::listCategory());
        $this->getShoppingCart();
        return view("front.index", [
            "arrivals"  => $this->productRepository->latestProduct(),
            "halls"     => $this->productVoteRepository->hallOfFame()
        ]);
    }

    public function a_propos()
    {
        return view("front.a_propos");
    }

    public function qualite()
    {
        return view("front.qualite");
    }

    public function contact()
    {
        return view('front.contact');
    }

    public function postContact(Request $request) {
        $info = (object) $request->all();
        try {
            Mail::to('contact@jewelcandle.shop')->send(new ContactMail($info));
            connectify('success', 'Nous contactez', "Votre message à bien été poster.<br>Nous y répondrons dès que possible.");

            return redirect()->back();
        }catch (\Exception $exception) {
            connectify('error', "Erreur", "Une erreur à eu lieu lors de l'envoi du message !");
            return redirect()->back();
        }
    }

    public function reclamation()
    {
        return view('front.reclamation');
    }

    private function getShoppingCart()
    {
        $_token = session()->get('_token');
        //dd($_token);
        if($this->cartRepository->isExits() == 0) {
            $this->cartRepository->initCart($_token);
        }
    }
}
