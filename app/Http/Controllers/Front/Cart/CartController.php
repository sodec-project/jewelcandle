<?php

namespace App\Http\Controllers\Front\Cart;

use App\Http\Controllers\Controller;
use App\Repository\Cart\CartProductRepository;
use App\Repository\Cart\CartRepository;
use App\Repository\Product\ProductCombinationRepository;
use App\Repository\Product\ProductRepository;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductCombinationRepository
     */
    private $productCombinationRepository;
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var CartProductRepository
     */
    private $cartProductRepository;

    /**
     * CartController constructor.
     * @param ProductRepository $productRepository
     * @param ProductCombinationRepository $productCombinationRepository
     * @param CartRepository $cartRepository
     * @param CartProductRepository $cartProductRepository
     */
    public function __construct(ProductRepository $productRepository, ProductCombinationRepository $productCombinationRepository, CartRepository $cartRepository, CartProductRepository $cartProductRepository)
    {
        $this->productRepository = $productRepository;
        $this->productCombinationRepository = $productCombinationRepository;
        $this->cartRepository = $cartRepository;
        $this->cartProductRepository = $cartProductRepository;
    }

    public function index()
    {
        return view("front.cart.index", [
            "cart"  => $this->cartRepository->getCart()
        ]);
    }

    public function addProduct(Request $request)
    {
        //dd($request->all());

        $cart = $this->cartRepository->getCart();
        foreach ($request->combination_price as $price) {
            $combi = $this->productCombinationRepository->getCombination($price);

            //Ont vérifie si l'article est déja dans le panier
            if ($this->cartProductRepository->exist($cart->id, $combi->product_id, $combi->id) == true) {
                // Si il existe ont met à jour la quantité +1
                $cartProduct = $this->cartProductRepository->get($cart->id, $combi->product_id, $combi->id);
                $this->cartProductRepository->update(
                    $cartProduct->id,
                    $this->updateQuantity($cartProduct->qte),
                    $this->updateTotalPrice($cartProduct->totalPrice, $this->updateQuantity($cartProduct->qte))
                );

                // Ont met à jour les information du panier
                $this->cartRepository->updateTotalCart($cart->id, $this->updateTotalCart($cart->id));

            } else {
                //Si il n'existe ont l'ajoute à la base de donnée
                $cartProduct = $this->cartProductRepository->create(
                    $cart->id,
                    $combi->product_id,
                    $combi->id,
                    $combi->product->price,
                    1,
                    $combi->product->price
                );

                // Mise à jour du panier
                $this->cartRepository->updateTotalCart($cart->id, $this->updateTotalCart($cart->id));
            }
        }

        connectify('success', 'Ajout d\'un produit', "Le produit <strong>".$combi->product->name."</strong> à été ajouter au panier");
        return redirect()->back();
    }

    private function updateQuantity($latestQuantity) {
        return $latestQuantity +1;
    }

    private function updateTotalPrice($latestPrice, $newQuantity) {
        return $latestPrice * $newQuantity;
    }

    private function updateTotalCart($cart_id)
    {
        return $this->cartProductRepository->updateTotalCart($cart_id);
    }
}
