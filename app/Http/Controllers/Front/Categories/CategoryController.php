<?php

namespace App\Http\Controllers\Front\Categories;

use App\Http\Controllers\Controller;
use App\Repository\Product\ProductCategoryRepository;
use App\Repository\Product\ProductRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var ProductCategoryRepository
     */
    private $productCategoryRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CategoryController constructor.
     * @param ProductCategoryRepository $productCategoryRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductCategoryRepository $productCategoryRepository, ProductRepository $productRepository)
    {
        $this->productCategoryRepository = $productCategoryRepository;
        $this->productRepository = $productRepository;
    }

    public function index($slug_category)
    {
        $category = $this->productCategoryRepository->getForSlug($slug_category);
        return view("front.product.category.index", [
            "category"  => $category,
            "products"  => $this->productRepository->getForCategory($category->id)
        ]);
    }
}
