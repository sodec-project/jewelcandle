<?php

namespace App\Http\Controllers\Front\Product;

use app\HelperClass\ProductClass;
use App\Http\Controllers\Controller;
use App\Repository\Product\ProductCategoryRepository;
use App\Repository\Product\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductCategoryRepository
     */
    private $productCategoryRepository;

    /**
     * ProductController constructor.
     * @param ProductRepository $productRepository
     * @param ProductCategoryRepository $productCategoryRepository
     */
    public function __construct(ProductRepository $productRepository, ProductCategoryRepository $productCategoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->productCategoryRepository = $productCategoryRepository;
    }

    public function index(Request $request)
    {
        return view('front.product.index', [
            "products"  => $this->productRepository->list(),
            "categories"    => $this->productCategoryRepository->list()
        ]);
    }

    public function show($product_id)
    {
        return view("front.product.show", [
            "product"   => $this->productRepository->getProduct($product_id)
        ]);
    }

    public function ajaxview($product_id)
    {
        $product = $this->productRepository->getProduct($product_id);
        ob_start();
        ?>
        <div class="single-product shop-quick-view-ajax clearfix">

            <div class="ajax-modal-title">
                <h2><?= $product->name ?></h2>
            </div>

            <div class="product modal-padding clearfix">

                <div class="col_half nobottommargin">
                    <div class="product-image">
                        <div class="fslider" data-pagi="false">
                            <div class="flexslider">
                                <div class="slider-wrap">
                                    <?php foreach ($product->images as $image): ?>
                                        <div class="slide">
                                            <a href="/storage/product/<?= $product_id; ?>.jpg"
                                               title="<?= $product->name; ?>">
                                                <img src="/storage/product/<?= $product_id; ?>.jpg"
                                                     alt="<?= $product->name; ?>">
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <!--<div class="sale-flash">Sale!</div>-->
                    </div>
                </div>
                <div class="col_half nobottommargin col_last product-desc">
                    <div class="product-price">
                        <ins><?= formatCurrency($product->price) ?></ins>
                    </div>
                    <?= ProductClass::getCountVote($product_id) ?>
                    <div class="clear"></div>
                    <div class="line"></div>

                    <a href="/product/<?= $product_id ?>" class="add-to-cart button nomargin">Voir le produit en
                        detail</a>

                    <div class="clear"></div>
                    <div class="line"></div>
                    <?= $product->summary ?>
                    <div class="panel panel-default product-meta nobottommargin">
                        <div class="panel-body">
                            <span itemprop="productID" class="sku_wrapper">SKU: <span
                                    class="sku"><?= $product->reference ?></span></span>
                            <span class="posted_in">Category: <a
                                    href="http://localhost/offbeat/wp/product-category/shoes/"
                                    rel="tag"><?= $product->subcategorie->name ?></a>.</span>
                            <span class="tagged_as">Tags: <a href="http://dante.swiftideas.net/product-tag/barena/"
                                                             rel="tag">Barena</a>, <a
                                    href="http://dante.swiftideas.net/product-tag/blazers/" rel="tag">Blazers</a>, <a
                                    href="http://dante.swiftideas.net/product-tag/tailoring/"
                                    rel="tag">Tailoring</a>, <a
                                    href="http://dante.swiftideas.net/product-tag/unconstructed/" rel="tag">Unconstructed</a>.</span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <?php
        $content = ob_get_clean();
        return response()->json($content);
    }
}
