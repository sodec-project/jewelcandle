<?php

namespace App\Http\Controllers\Back\Catalog\Category;

use App\Http\Controllers\Controller;
use App\Repository\Product\ProductCategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var ProductCategoryRepository
     */
    private $productCategoryRepository;

    /**
     * CategoryController constructor.
     * @param ProductCategoryRepository $productCategoryRepository
     */
    public function __construct(ProductCategoryRepository $productCategoryRepository)
    {
        $this->productCategoryRepository = $productCategoryRepository;
    }

    public function index()
    {
        return view("back.catalogue.category.index", [
            "categories"    => $this->productCategoryRepository->list()
        ]);
    }
}
