<?php

namespace App\Http\Controllers\Api\Cart;

use App\Http\Controllers\Controller;
use App\Repository\Cart\CartRepository;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * CartController constructor.
     * @param CartRepository $cartRepository
     */
    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function getTotal()
    {
        $total = $this->cartRepository->getCart();
        return response()->json(["total" => formatCurrency($total->total + 9.90)]);
    }

    public function firstTotal()
    {
        $total = $this->cartRepository->getCart();
        return response()->json(["total" => formatCurrency($total->total)]);
    }
}
