<?php

namespace App\Http\Controllers\Api\Cart;

use App\Http\Controllers\Controller;
use App\Repository\Cart\CartProductRepository;
use App\Repository\Cart\CartRepository;
use Illuminate\Http\Request;

class CartProductController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var CartProductRepository
     */
    private $cartProductRepository;

    /**
     * CartProductController constructor.
     * @param CartRepository $cartRepository
     * @param CartProductRepository $cartProductRepository
     */
    public function __construct(CartRepository $cartRepository, CartProductRepository $cartProductRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->cartProductRepository = $cartProductRepository;
    }

    public function delete($cartProduct_id)
    {
        $cart = $this->cartRepository->getCart();
        $product = $this->cartProductRepository->getProduct($cartProduct_id);

        try {
            // Mise à jour du total du panier
            $newTotal = $cart->total - $product->totalPrice;
            $this->cartRepository->updateTotalCart($cart->id, $newTotal);
            // Suppression du produit du panier
            $this->cartProductRepository->delete($cartProduct_id);
            return response()->json(["code" => 200]);
        }catch (\Exception $exception) {
            return response()->json(["code" => 500, "message" => $exception->getMessage()]);
        }
    }

    public function updateCart(Request $request) {
        dd($request->all());
    }

    private function updateQuantity($latestQuantity) {
        return $latestQuantity +1;
    }

    private function updateTotalPrice($latestPrice, $newQuantity) {
        return $latestPrice * $newQuantity;
    }

    private function updateTotalCart($cart_id)
    {
        return $this->cartProductRepository->updateTotalCart($cart_id);
    }
}
