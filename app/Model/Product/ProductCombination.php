<?php

namespace App\Model\Product;

use App\Model\Cart\CartProduct;
use Illuminate\Database\Eloquent\Model;

class ProductCombination extends Model
{
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function cartproducts()
    {
        return $this->belongsTo(CartProduct::class);
    }
}
