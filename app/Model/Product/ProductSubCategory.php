<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductSubCategory extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo(ProductCategorie::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
