<?php

namespace App\Model\Product;

use App\Model\Cart\CartProduct;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function categorie()
    {
        return $this->belongsTo(ProductCategorie::class, 'product_categorie_id');
    }

    public function subcategorie()
    {
        return $this->belongsTo(ProductSubCategory::class, 'product_sub_categorie_id');
    }

    public function combinations()
    {
        return $this->hasMany(ProductCombination::class);
    }

    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function votes()
    {
        return $this->hasMany(ProductVote::class);
    }

    public function cartproducts()
    {
        return $this->hasMany(CartProduct::class);
    }
}
