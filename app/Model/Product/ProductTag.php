<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
