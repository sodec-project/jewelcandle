<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductInventory extends Model
{
    protected $guarded = [];

    public function getDates()
    {
        return ["inventory_date"];
    }
}
