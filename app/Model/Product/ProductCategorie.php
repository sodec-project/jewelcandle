<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductCategorie extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function subcategories()
    {
        return $this->hasMany(ProductSubCategory::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
