<?php

namespace App\Model\Product;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ProductVote extends Model
{
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
