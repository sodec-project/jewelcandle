<?php

namespace App\Model\Improve\Carrier;

use App\Model\Product\ProductShipping;
use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function productShippings()
    {
        return $this->hasMany(ProductShipping::class);
    }
}
