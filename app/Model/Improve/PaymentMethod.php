<?php

namespace App\Model\Improve;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
