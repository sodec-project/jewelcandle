<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model
{
    protected $guarded = [];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public function getDates()
    {
        return ["datePayment"];
    }
}
