<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }

    public function payments()
    {
        return $this->hasMany(OrderPayment::class);
    }

    public function deliveries()
    {
        return $this->hasMany(OrderDelivery::class);
    }

    public function returns()
    {
        return $this->hasMany(OrderReturn::class);
    }
}
