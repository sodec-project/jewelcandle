<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;

class OrderState extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
