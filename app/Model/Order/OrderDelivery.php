<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;

class OrderDelivery extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
