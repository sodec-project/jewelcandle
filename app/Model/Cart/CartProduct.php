<?php

namespace App\Model\Cart;

use App\Model\Product\Product;
use App\Model\Product\ProductCombination;
use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function combination()
    {
        return $this->belongsTo(ProductCombination::class, 'combination_id');
    }
}
