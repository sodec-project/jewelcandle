<?php

namespace App\Model\Cart;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded = [];

    public function cartproducts()
    {
        return $this->hasMany(CartProduct::class);
    }
}
