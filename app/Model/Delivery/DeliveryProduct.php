<?php

namespace App\Model\Delivery;

use Illuminate\Database\Eloquent\Model;

class DeliveryProduct extends Model
{
    protected $guarded = [];
}
