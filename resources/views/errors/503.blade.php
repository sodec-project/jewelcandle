@extends("front.layout.app")

@section("content")
    <section id="content">
        <div class="content-wrap">
            <div class="container subscribe-widget clearfix">
                <div class="heading-block title-center nobottomborder">
                    <h1>Nous somme actuellement en maintenance</h1>
                    <span>Nous nous excusons pour la gène occasionnée</span>
                </div>
                <div id="countdown-ex1" class="countdown countdown-large divcenter bottommargin is-countdown" style="max-width:700px;"></div>
                <div class="clear"></div>
                <div class="progress topmargin divcenter" style="height: 10px; max-width:600px;">
                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                        <span class="sr-only">80% Complete</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("script")
    <script type="text/javascript">

        jQuery(document).ready( function($){
            let newDate = new Date(2020, 1, 20);
            $('#countdown-ex1').countdown({until: newDate});
        });

    </script>
@endsection
