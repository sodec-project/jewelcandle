<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>Nouvelle Prise de contact</p>
    <table>
        <tr>
            <td>Identifiant:</td>
            <td>{{ $info->nom }} {{ $info->prenom }}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{ $info->email }}</td>
        </tr>
        <tr>
            <td>Téléphone:</td>
            <td>{{ $info->telephone }}</td>
        </tr>
        <tr>
            <td>Message:</td>
            <td>{{ $info->message }}</td>
        </tr>
    </table>
</body>
</html>
