@extends("front.layout.app")

@section("content")
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_one_third nobottommargin">
                <div class="well well-lg nobottommargin">
                    <form id="login-form" name="login-form" class="nobottommargin" action="{{ route('login') }}" method="post">
                        @csrf
                        <h3>Connexion à votre compte</h3>
                        <div class="col_full">
                            <label for="login-form-username">Adresse Mail:</label>
                            <input type="text" id="login-form-username" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="col_full">
                            <label for="login-form-password">Mot de Passe:</label>
                            <input type="password" id="login-form-password" name="password" value="" class="form-control @error('password') is-invalid @enderror">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="col_full">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    Se souvenir de moi
                                </label>
                            </div>
                        </div>
                        <div class="col_full nobottommargin">
                            <button type="submit" class="button button-3d nomargin" id="login-form-submit" name="login-form-submit" value="login">Connexion</button>
                            <a href="{{ route('password.request') }}" class="fright">Mot de passe perdu ?</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col_two_third col_last nobottommargin">
                <h3>Vous n'avez pas de compte ? Inscrivez-vous</h3>

                <form id="register-form" name="register-form" class="nobottommargin" action="{{ route('register') }}" method="post">
                    @csrf
                    <div class="col_half">
                        <label for="register-form-name">Votre Nom & Prénom:</label>
                        <input type="text" id="register-form-name" name="name" value="{{ old("name") }}" class="form-control @error('name') is-invalid @enderror">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col_half col_last">
                        <label for="register-form-email">Adresse Mail:</label>
                        <input type="text" id="register-form-email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="clear"></div>
                    <div class="col_half">
                        <label for="register-form-password">Mot de passe:</label>
                        <input type="password" id="register-form-password" name="password" value="" class="form-control @error('password') is-invalid @enderror">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="col_half col_last">
                        <label for="register-form-repassword">Confirmation du mot de passe:</label>
                        <input type="password" id="register-form-repassword" name="password_confirmation" value="" class="form-control">
                    </div>
                    <div class="clear"></div>
                    <div class="col_full nobottommargin">
                        <button type="submit" class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">M'inscrire</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
