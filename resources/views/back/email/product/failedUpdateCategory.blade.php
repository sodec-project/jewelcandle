@extends('beautymail::templates.widgets')

@section('content')

    @include('beautymail::templates.widgets.articleStart')

    <h4 class="secondary"><strong>Une Erreur à été détécter par le système JOB</strong></h4>
    <strong>JOB:</strong> UpdatedCategory<br>
    <strong>Erreur:</strong>
    <p>{{ $exception }}</p>

    @include('beautymail::templates.widgets.articleEnd')

@stop
