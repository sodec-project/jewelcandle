@extends("back.layout.app")
@section("style")

@endsection

@section("bread")
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    Liste des Catégories </h3>
                <span class="kt-subheader__separator kt-hidden"></span>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="kt-portlet">
    	<div class="kt-portlet__head">
    		<div class="kt-portlet__head-label">
    			<span class="kt-portlet__head-icon">
    				<i class="flaticon2-box-1"></i>
    			</span>
    			<h3 class="kt-portlet__head-title kt-font-primary">
    				Liste des catégories
    			</h3>
    		</div>
    		<div class="kt-portlet__head-toolbar">
    			<div class="kt-portlet__head-actions">
    				<a href="" class="btn btn-primary"><i class="fa fa-plus"></i> Ajouter une catégorie</a>
    			</div>
    		</div>
    	</div>
    	<div class="kt-portlet__body">
    		<table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Désignation</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->name }}</td>
                            <td>
                                <button type="button" class="btn btn-primary btn-icon"><i class="fa fa-edit"></i></button>
                                <button type="button" class="btn btn-danger btn-icon"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
    	</div>
    </div>
@endsection

@section('script')

@endsection
