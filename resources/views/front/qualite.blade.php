@extends("front.layout.app")

@section("content")
    <section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-1.jpg'); background-size: cover; background-position: center center;" data-stellar-background-ratio="0.4">

        <div class="container clearfix">
            <h1>Engagement qualité</h1>
            <span>Votre JewelCandle – la meilleure qualité à tout point de vue!</span>
            <ol class="breadcrumb">
                <li><a href="#">Acceuil</a></li>
                <li class="active">Engagement qualité</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-1.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>1. Production</h2>
                            </div>
                            <p>Chacune de nos bougies parfumées est produite en Europe. Nous ne fabriquons pas nos produits dans des pays à bas coût, dans des ateliers illégaux et nous n’utilisons pas l’aide de machines automatisées énormes. Chaque JewelCandle est synonyme d’amour.</p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-2.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>2. Design</h2>
                            </div>
                            <p>Les images correspondantes aux senteurs respectives sur le contenant en verre des bougies sont dessinées et peintes à la main par nos artistes. Elles sont donc uniques et ne se trouvent que sur les originaux de JewelCandle.</p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-3.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>3. Matières Premières</h2>
                            </div>
                            <p>Pour votre JewelCandle nous utilisons une peinture sans cadmium et sans plastifiant, des mèches sans plomb et une cire sans soufre : l’environnement est important pour nous!</p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-4.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>4. Personnalité</h2>
                            </div>
                            <p>Avant de recevoir son packaging, chaque JewelCandle est une fois de plus inspectée avec attention. C’est seulement lorsque tout est parfait que les bougies parfumées reçoivent la signature personnelle de l’employé responsable. Vous la trouverez dans la petite brochure attachée à chaque JewelCandle.</p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-5.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>5. Détail</h2>
                            </div>
                            <p>Chaque nœud est serré à la main. Il donne à votre JewelCandle la touche finale et permet à votre bougie parfumée de toujours avoir un aspect impeccable.</p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-6.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>6. Bijoux</h2>
                            </div>
                            <p>Bien sûr, nous prêtons une attention particulière à choisir nos merveilleux bijoux. Pour vous, c’est la garantie que dans chaque bougie parfumée il n’y a pas de bijoux bon marché mais des bijoux exclusivement faits d’argent véritable.</p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-7.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>7. Sécurité</h2>
                            </div>
                            <p>ous prenons toutes les précautions nécessaires pour protéger vos bijoux. Chaque bijou est emballé dans un sachet zippé puis entouré dans un aluminium résistant à la chaleur avant d’être caché dans la cire parfumée.</p>
                        </div>

                    </div>

                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-quali-8.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>8. Packaging</h2>
                            </div>
                            pEn plus, une boîte de protection a été développée par nos experts spécialement pour votre JewelCandle. Elle protège chacune de nos précieuses bougies parfumées des fortes vibrations lors du transport et permet à toutes nos bougies et bijoux d’arriver à votre porte dans toute leur splendeur.
                        </div>

                    </div>

                </div>
            </div>
            <div class="promo promo-dark promo-full promo-uppercase bottommargin">
                <div class="container clearfix">
                    <h3>Si, malgré toutes nos précautions, un détail ne correspond pas à vos attentes, n’hésitez pas à contacter notre service clientèle – peu importe le problème, nous trouverons une solution ensemble!</h3>
                </div>
            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section("script")
<script type="text/javascript">
    jQuery(document).ready( function($){
        $('#shop').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                },
                price_hl: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
            var element = $(this),
                elementFilter = element.children('a').attr('data-filter'),
                elementFilterContainer = element.parents('.custom-filter').attr('data-container');

            elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

            element.append('<span>'+ elementFilterCount +'</span>');

        });

        $('.shop-sorting li').click( function() {
            $('.shop-sorting').find('li').removeClass( 'active-filter' );
            $(this).addClass( 'active-filter' );
            var sortByValue = $(this).find('a').attr('data-sort-by');
            $('#shop').isotope({ sortBy: sortByValue });
            return false;
        });
    });
</script>
@endsection
