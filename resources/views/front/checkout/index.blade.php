@extends("front.layout.app")

@section("content")
    <section id="page-title">

        <div class="container clearfix">
            <h1>Mon Panier</h1>
            <ol class="breadcrumb">
                <li><a href="#">Accueil</a></li>
                <li class="active">Passage de commande</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->

    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">
                <div id="processTabs" class="ui-tabs ui-corner-all ui-widget ui-widget-content">
                    <ul class="process-steps bottommargin clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
                        <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="ptab1" aria-labelledby="ui-id-1" aria-selected="true" aria-expanded="true">
                            <a href="#ptab1" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">1</a>
                            <h5>Mon Panier</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab2" aria-labelledby="ui-id-2" aria-selected="false" aria-expanded="false">
                            <a href="#ptab2" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">2</a>
                            <h5>Livraison</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab3" aria-labelledby="ui-id-3" aria-selected="false" aria-expanded="false">
                            <a href="#ptab3" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-3">3</a>
                            <h5>Paiement</h5>
                        </li>
                        <li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="ptab4" aria-labelledby="ui-id-4" aria-selected="false" aria-expanded="false">
                            <a href="#ptab4" class="i-circled i-bordered i-alt divcenter ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-4">4</a>
                            <h5>Commande Terminer</h5>
                        </li>
                    </ul>
                    <div>
                        <div id="ptab1" aria-labelledby="ui-id-1" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false" style="">
                            <p>Toutes commandes passée avant 11H est envoyer le jour même suivant les stocks disponible.</p>
                            <div class="table-responsive">
                                <table class="table cart">
                                    <thead>
                                    <tr>
                                        <th class="cart-product-remove">&nbsp;</th>
                                        <th class="cart-product-thumbnail">&nbsp;</th>
                                        <th class="cart-product-name">Produit</th>
                                        <th class="cart-product-price">Prix unitaire</th>
                                        <th class="cart-product-quantity">Quantité</th>
                                        <th class="cart-product-subtotal">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cart->cartProducts as $product)
                                    <tr class="cart_item">
                                        <td class="cart-product-remove">
                                            <a href="#" class="remove" title="Remove this item"><i class="icon-trash2"></i></a>
                                        </td>
                                        <td class="cart-product-thumbnail">
                                            <a href="{{ route('Front.Product.show', $product->product_id) }}"><img width="64" height="64" src="/storage/product/{{ $product->product_id }}.jpg" alt="Pink Printed Dress"></a>
                                        </td>
                                        <td class="cart-product-name">
                                            <a href="#">{{ $product->product->name }} - {!! \app\HelperClass\ProductClass::iconCombination($product->combination->name) !!} {{ $product->combination->name }}</a>
                                        </td>
                                        <td class="cart-product-price">
                                            <span class="amount">{{ formatCurrency($product->unitPrice) }}</span>
                                        </td>
                                        <td class="cart-product-quantity">
                                            <div class="quantity clearfix">
                                                <input class="product_id" type="hidden" name="product_id" value="{{ $product->id }}">
                                                <input type="text" name="quantity" value="{{ $product->qte }}"
                                                       class="qty product_qte" disabled/>
                                            </div>
                                        </td>
                                        <td class="cart-product-subtotal">
                                            <span class="amount">{{ formatCurrency($product->totalPrice) }}</span>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <a href="" class="button button-3d nomargin fright tab-linker" rel="2">Livraison ⇒</a>
                        </div>
                        <div id="ptab2" aria-labelledby="ui-id-2" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true" style="display: none;">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt, deleniti, atque soluta ratione blanditiis maxime at architecto laudantium eius eaque distinctio dolorem voluptatem nam ab molestias velit nemo. Illo, hic.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, modi, odit, aspernatur veritatis ipsum molestiae impedit iusto blanditiis voluptatem ab voluptas ullam expedita repellendus porro assumenda non deserunt repellat eius rem dolorem corporis temporibus voluptatibus ut! Quod, corporis, tempora, dolore, sint earum minus deserunt eveniet natus error magnam aliquam nemo.</p>
                            <div class="line"></div>
                            <a href="#" class="button button-3d nomargin tab-linker" rel="1">Previous</a>
                            <a href="#" class="button button-3d nomargin fright tab-linker" rel="3">Pay Now</a>
                        </div>
                        <div id="ptab3" aria-labelledby="ui-id-3" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true" style="display: none;">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis, sit, culpa, placeat, tempora quibusdam molestiae cupiditate atque tempore nemo tenetur facere voluptates autem aliquid provident distinctio beatae odio maxime pariatur eos ratione quae itaque quod. Distinctio, temporibus, cupiditate, eaque vero illo molestiae vel doloremque dolorum repellat ullam possimus modi dicta eum debitis ratione est in sunt et corrupti adipisci quibusdam praesentium optio laborum tempora ipsam aut cum consectetur veritatis dolorem.</p>
                            <div class="line"></div>
                            <a href="#" class="button button-3d nomargin tab-linker" rel="2">Previous</a>
                            <a href="#" class="button button-3d nomargin fright tab-linker" rel="4">Complete Order</a>
                        </div>
                        <div id="ptab4" aria-labelledby="ui-id-4" role="tabpanel" class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true" style="display: none;">
                            <div class="alert alert-success">
                                <strong>Thank You.</strong> Your order will be processed once we verify the Payment.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section><!-- #content end -->

@endsection

@section("script")
    <script src="/assets/front/js/pages/cart/index.js" type="text/javascript"></script>
    <script>
        $(function() {
            $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
            $( ".tab-linker" ).click(function() {
                $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
                return false;
            });
        });
    </script>
@endsection
