@extends("front.layout.app")

@section("content")
    <section id="page-title">

        <div class="container clearfix">
            <h1>Nos Produits</h1>
            <ol class="breadcrumb">
                <li><a href="#">Accueil</a></li>
                <li><a href="#">Produits</a></li>
                <li class="active">Tous nos produits</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <!-- Post Content
                ============================================= -->
                <div class="postcontent nobottommargin col_last">

                    <!-- Shop
                    ============================================= -->
                    <div id="shop" class="shop product-3 grid-container clearfix">
                        @foreach($products as $product)
                        <div class="product sf-{{ \Illuminate\Support\Str::slug($product->subcategorie->name) }} clearfix">
                            <div class="product-image">
                                <a href="{{ route('Front.Product.show', $product->id) }}"><img src="/storage/product/{{ $product->id }}.jpg" alt="{{ $product->name }}"></a>
                            </div>
                            <div class="product-desc center">
                                <div class="product-title"><h3><a href="{{ route('Front.Product.show', $product->id) }}">{{ $product->name }}</a></h3></div>
                                <div class="product-price"><ins>{{ formatCurrency($product->price) }}</ins></div>
                                {!! \app\HelperClass\ProductClass::getCountVote($product->id) !!}
                            </div>
                        </div>
                        @endforeach

                    </div><!-- #shop end -->

                </div><!-- .postcontent end -->

                <!-- Sidebar
                ============================================= -->
                <div class="sidebar nobottommargin">
                    <div class="sidebar-widgets-wrap">

                        <div class="widget widget-filter-links clearfix">

                            <h4>Catégories</h4>
                            <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
                                <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Effacer</a></li>
                                @foreach($categories as $category)
                                    @if(\App\Repository\Product\ProductSubCategoryRepository::getCountForCategory($category->id) == 0)
                                        <li><a href="#" data-filter=".sf-{{ \Illuminate\Support\Str::slug($category->name)  }}">{{ $category->name }}</a></li>
                                    @else
                                        <li>
                                            <a href="#" data-filter=".sf-{{ \Illuminate\Support\Str::slug($category->name)  }}">{{ $category->name }}</a>
                                            <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
                                                @foreach($category->subcategories as $subcategory)
                                                <li><a href="#" data-filter=".sf-{{ \Illuminate\Support\Str::slug($subcategory->name) }}">{{ $subcategory->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>

                        </div>

                        <div class="widget widget-filter-links clearfix">

                            <h4>Sort By</h4>
                            <ul class="shop-sorting">
                                <li class="widget-filter-reset active-filter"><a href="#" data-sort-by="original-order">Clear</a></li>
                                <li><a href="#" data-sort-by="name">Name</a></li>
                                <li><a href="#" data-sort-by="price_lh">Price: Low to High</a></li>
                                <li><a href="#" data-sort-by="price_hl">Price: High to Low</a></li>
                            </ul>

                        </div>

                    </div>
                </div><!-- .sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section("script")
<script type="text/javascript">
    jQuery(document).ready( function($){
        $('#shop').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                },
                price_hl: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
            var element = $(this),
                elementFilter = element.children('a').attr('data-filter'),
                elementFilterContainer = element.parents('.custom-filter').attr('data-container');

            elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

            element.append('<span>'+ elementFilterCount +'</span>');

        });

        $('.shop-sorting li').click( function() {
            $('.shop-sorting').find('li').removeClass( 'active-filter' );
            $(this).addClass( 'active-filter' );
            var sortByValue = $(this).find('a').attr('data-sort-by');
            $('#shop').isotope({ sortBy: sortByValue });
            return false;
        });
    });
</script>
@endsection
