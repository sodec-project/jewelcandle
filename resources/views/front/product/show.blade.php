@extends("front.layout.app")

@section("style")
    <link rel="stylesheet" href="/assets/front/css/components/bs-switches.css" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/components/radio-checkbox.css" type="text/css" />
@endsection

@section("content")
    <section id="page-title">

        <div class="container clearfix">
            <h1>{{ $product->name }}</h1>
            <ol class="breadcrumb">
                <li><a href="#">Accueil</a></li>
                <li><a href="#">Produits</a></li>
                <li class="active">{{ $product->name }}</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->

    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="single-product">

                    <div class="product">

                        <div class="col_two_fifth">

                            <!-- Product Single - Gallery
                            ============================================= -->
                            <div class="product-image">
                                <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                    <div class="flexslider">
                                        <div class="slider-wrap" data-lightbox="gallery">
                                            @foreach($product->images as $image)
                                                <div class="slide" data-thumb="/storage/product/{{ $image->product_id }}.jpg">
                                                    <a href="/storage/product/{{ $image->product_id }}.jpg" title="{{ $product->name }}" data-lightbox="gallery-item">
                                                        <img src="/storage/product/{{ $image->product_id }}.jpg" alt="{{ $product->name }}">
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!--<div class="sale-flash">Sale!</div>-->
                            </div><!-- Product Single - Gallery End -->

                        </div>

                        <div class="col_three_fifth product-desc col_last">

                            <!-- Product Single - Price
                            ============================================= -->
                            <div class="product-price" id="price"><ins>{{ formatCurrency($product->price) }}</ins></div><!-- Product Single - Price End -->

                            <!-- Product Single - Rating
                            ============================================= -->
                            {!! \app\HelperClass\ProductClass::getCountVote($product->id) !!}

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Quantity & Cart Button
                            ============================================= -->
                            <form class="cart nobottommargin clearfix" action="{{ route('Cart.addProduct') }}" method="post" enctype='multipart/form-data'>
                                @csrf

                                <h4>BIJOU</h4>
                                @foreach($product->combinations as $comb)
                                    <div>
                                        <input id="checkbox-{{ $comb->id }}" class="checkbox-style" name="combination_price[]" type="checkbox" @if($comb->quantity == 0) disabled="" @endif value="{{ $comb->id }}">
                                        <label for="checkbox-{{ $comb->id }}" class="checkbox-style-2-label @if($comb->quantity == 0) disabled @endif" @if($comb->quantity == 0) data-toggle="tooltip" title="Indisponible" @endif> {!! \app\HelperClass\ProductClass::iconCombination($comb->name) !!} {{ $comb->name }}</label>
                                    </div>
                                @endforeach
                                <div class="line"></div>
                                <button type="submit" class="add-to-cart button nomargin">Ajouter au panier</button>
                            </form><!-- Product Single - Quantity & Cart Button End -->

                            <div class="clear"></div>
                            <div class="line"></div>

                            <!-- Product Single - Short Description
                            ============================================= -->
                        {{ $product->summary }}

                        <!-- Product Single - Meta
                            ============================================= -->
                            <div class="panel panel-default product-meta">
                                <div class="panel-body">
                                    <span itemprop="productID" class="sku_wrapper">SKU: <span class="sku">{{ $product->reference }}</span></span>
                                    <span class="posted_in">Category: <a href="#" rel="tag">{{ $product->subcategorie->name }}</a>.</span>
                                    <span class="tagged_as">Tags: <a href="#" rel="tag">Pink</a>, <a href="#" rel="tag">Short</a>, <a href="#" rel="tag">Dress</a>, <a href="#" rel="tag">Printed</a>.</span>
                                </div>
                            </div><!-- Product Single - Meta End -->

                            <!-- Product Single - Share
                            ============================================= -->
                            <div class="si-share noborder clearfix">
                                <span>Partager:</span>
                                <div>
                                    <a href="#" class="social-icon si-borderless si-facebook">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-twitter">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>
                                    <a href="#" class="social-icon si-borderless si-email3">
                                        <i class="icon-email3"></i>
                                        <i class="icon-email3"></i>
                                    </a>
                                </div>
                            </div><!-- Product Single - Share End -->

                        </div>

                        <div class="col_full nobottommargin">

                            <div class="tabs clearfix nobottommargin" id="tab-1">

                                <ul class="tab-nav clearfix">
                                    <li><a href="#tabs-1"><i class="icon2-candle"></i><span class="hidden-xs"> Description</span></a></li>
                                    <li><a href="#tabs-2"><i class="icon2-product"></i><span class="hidden-xs"> Conseils d'utilisation et détails</span></a></li>
                                    <li><a href="#tabs-3"><i class="icon2-small-diamond"></i><span class="hidden-xs"> Bijoux</span></a></li>
                                    <!--<li><a href="#tabs-2"><i class="icon-info-sign"></i><span class="hidden-xs"> Additional Information</span></a></li>-->
                                    <!--<li><a href="#tabs-3"><i class="icon-star3"></i><span class="hidden-xs"> Reviews (2)</span></a></li>-->
                                </ul>

                                <div class="tab-container">

                                    <div class="tab-content clearfix" id="tabs-1">
                                        <span style="font-weight: bold;"><span style="color: #4d2463">Jewel</span>Candle {{ $product->name }}</span>
                                        {!! $product->description !!}
                                    </div>
                                    <div class="tab-content clearfix" id="tabs-2">
                                        <h3 style="font-weight: bold;">Conseils d’utilisation </h3>
                                        <ol>
                                            <li>Afin que votre bougie brûle uniformément, veuillez la laisser brûler au moins une heure pendant le premier allumage.<br>
                                                Ne laissez jamais une bougie allumée sans surveillance !</li>
                                            <li>Avant de rallumer votre bougie (après le premier allumage), coupez la mèche de 6 mm au-dessus de la cire. Cela permet d’obtenir une flamme propre et plus petite.</li>
                                            <li>Ne laissez pas votre bougie brûler plus de 4 heures.</li>
                                        </ol>
                                        <h3 style="font-weight: bold;">Détails</h3>
                                        <p>Tous nos produits sont fabriqués et contrôlés avec la plus grande attention afin de vous assurer une expérience de la meilleure qualité.</p>
                                        <table class="table table-bordered">
                                            <thead style="background: #4d2463; color: white">
                                                <tr>
                                                    <th>Type de Bougie</th>
                                                    <th>Hauteur</th>
                                                    <th>Diamètre</th>
                                                    <th>Poids</th>
                                                    <th>Tps de combustions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Grande</td>
                                                    <td>17 cm</td>
                                                    <td>8 cm</td>
                                                    <td>1,050 Kg</td>
                                                    <td>Entre 90 et 150H</td>
                                                </tr>
                                                <tr>
                                                    <td>Petite</td>
                                                    <td>12 cm</td>
                                                    <td>8 cm</td>
                                                    <td>0,800 Kg</td>
                                                    <td>Entre 45 et 65H</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-content clearfix" id="tabs-3">
                                        <p>Dans chacune de nos bougies parfumées de qualité, faites entièrement à la main, se trouve un bijou surprise.</p>
                                        <h3 style="font-weight: bold;">Comment connaître la valeur du bijou ?</h3>
                                        <p>Tous nos bijoux portent une étiquette avec un code numérique à 13 chiffres qui vous permet de trouver la valeur approximative * de votre bijou sur notre <a href="{{ route('Front.valeur') }}">site</a> .<br>
                                            La valeur du bijou que vous trouverez dans votre bougie est une surprise autant pour vous que pour nous.</p>
                                        <h6>
                                            *La valeur affichée correspond au prix de vente conseillé du fabricant pour la vente aux clients finaux. Afin de déterminer une valeur plus précise de votre bijou, nous vous invitons à consulter un bijoutier de votre région.
                                        </h6>
                                        <div class="line"></div>
                                        <h3 style="font-weight: bold;">La bague est-elle à ma taille ?</h3>
                                        <p>Nos avons 3 tailles de bagues disponibles (S, M ou L). Vous pouvez vous référer au tableau ci-dessous.</p>
                                        <table class="table table-bordered">
                                            <thead style="background: #4d2463; color: white">
                                                <tr>
                                                    <th>Catégorie</th>
                                                    <th>Taille</th>
                                                    <th>Diamètre en mm</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>XS</td>
                                                    <td>49-51</td>
                                                    <td>15,6 - 16,2</td>
                                                </tr>
                                                <tr>
                                                    <td>S</td>
                                                    <td>52-54</td>
                                                    <td>16,6 - 17,2</td>
                                                </tr>
                                                <tr>
                                                    <td>M</td>
                                                    <td>55-57</td>
                                                    <td>17,3 - 18,1</td>
                                                </tr>
                                                <tr>
                                                    <td>L</td>
                                                    <td>58-60</td>
                                                    <td>18,2 - 19,1</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div class="line"></div>
                                        <h3 style="font-weight: bold;">Puis-je choisir mon bijou ?</h3>
                                        <p>
                                            Vous pouvez choisir le type de votre bijou. C’est-à-dire, vous avez le choix entre bague taille XS, S, M, L, boucles d’oreilles, collier, charm ou bracelet. Par contre, vous ne pouvez pas choisir le modèle du bijou car c’est une surprise. Ils sont introduits dans la bougie lors de la production. Le bijou que vous trouvez dans votre bougie n’est pas nécessairement celui de la photo mais tous les bijoux présents sur nos photos font bien partie de notre collection.
                                        </p>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="clear"></div><div class="line"></div>

                <div class="col_full nobottommargin">

                    <h4>Related Products</h4>

                    <div id="oc-product" class="owl-carousel product-carousel carousel-widget" data-margin="30" data-pagi="false" data-autoplay="5000" data-items-xxs="1" data-items-sm="2" data-items-md="3" data-items-lg="4">

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="images/shop/dress/1.jpg" alt="Checked Short Dress"></a>
                                    <a href="#"><img src="images/shop/dress/1-1.jpg" alt="Checked Short Dress"></a>
                                    <div class="sale-flash">50% Off*</div>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Checked Short Dress</a></h3></div>
                                    <div class="product-price"><del>$24.99</del> <ins>$12.49</ins></div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="images/shop/pants/1-1.jpg" alt="Slim Fit Chinos"></a>
                                    <a href="#"><img src="images/shop/pants/1.jpg" alt="Slim Fit Chinos"></a>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Slim Fit Chinos</a></h3></div>
                                    <div class="product-price">$39.99</div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-half-full"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="images/shop/shoes/1-1.jpg" alt="Dark Brown Boots"></a>
                                    <a href="#"><img src="images/shop/shoes/1.jpg" alt="Dark Brown Boots"></a>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Dark Brown Boots</a></h3></div>
                                    <div class="product-price">$49</div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-empty"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="images/shop/dress/2.jpg" alt="Light Blue Denim Dress"></a>
                                    <a href="#"><img src="images/shop/dress/2-2.jpg" alt="Light Blue Denim Dress"></a>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Light Blue Denim Dress</a></h3></div>
                                    <div class="product-price">$19.95</div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="oc-item">
                            <div class="product iproduct clearfix">
                                <div class="product-image">
                                    <a href="#"><img src="images/shop/sunglasses/1.jpg" alt="Unisex Sunglasses"></a>
                                    <a href="#"><img src="images/shop/sunglasses/1-1.jpg" alt="Unisex Sunglasses"></a>
                                    <div class="sale-flash">Sale!</div>
                                    <div class="product-overlay">
                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Add to Cart</span></a>
                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Quick View</span></a>
                                    </div>
                                </div>
                                <div class="product-desc center">
                                    <div class="product-title"><h3><a href="#">Unisex Sunglasses</a></h3></div>
                                    <div class="product-price"><del>$19.99</del> <ins>$11.99</ins></div>
                                    <div class="product-rating">
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star3"></i>
                                        <i class="icon-star-empty"></i>
                                        <i class="icon-star-empty"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section("script")
<script type="text/javascript">

</script>
@endsection
