@include("front.layout.includes.heading")

<body class="stretched">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">
    @include("front.layout.includes.topbar")
    @include("front.layout.includes.header")

    @yield("content")

    @include("front.layout.includes.footer")

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="/assets/front/js/jquery.js"></script>
<script type="text/javascript" src="/assets/front/js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/assets/front/js/functions.js"></script>

<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="/assets/front/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script>
    function initFreshChat() {
        window.fcWidget.init({
            token: "ec6da932-5626-4fbd-b35e-9a41e4ab543d",
            host: "https://wchat.freshchat.com"
        });
    }
    function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@include('notify::messages')
@notifyJs
@yield("script")
</body>
</html>
