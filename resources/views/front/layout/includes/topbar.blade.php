<!-- Top Bar
    ============================================= -->
<div id="top-bar" class="hidden-xs">

    <div class="container clearfix">

        <div class="col_half nobottommargin">

            <p class="nobottommargin"><strong>Téléphone:</strong> 0 899 492 648 | <strong>Email:</strong> contact@jewelcandle.shop</p>

        </div>

        <div class="col_half col_last fright nobottommargin">

            <!-- Top Links
            ============================================= -->
            <div class="top-links">
                <ul>
                    @auth()
                        <li>
                            <a href=""><i class="icon-user2"></i> Mon Compte</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" style="background-color: #e93b3b; color: white;"><i class="icon-line2-logout"></i> Déconnexion</a>
                        </li>
                    @else
                        <li><a href="#">Connexion</a>
                            <div class="top-link-section">
                                <form id="top-login" role="form" action="{{ route('login') }}" method="post">
                                    @csrf
                                    <div class="input-group" id="top-login-username">
                                        <span class="input-group-addon"><i class="icon-user"></i></span>
                                        <input type="email" name="email" class="form-control" placeholder="Adresse Mail" required="">
                                    </div>
                                    <div class="input-group" id="top-login-password">
                                        <span class="input-group-addon"><i class="icon-key"></i></span>
                                        <input type="password" name="password" class="form-control" placeholder="Mot de Passe" required="">
                                    </div>
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Se souvenir de moi
                                    </label>
                                    <button class="btn btn-danger btn-block" type="submit">Connexion</button>
                                </form>
                            </div>
                        </li>
                    @endauth
                </ul>
            </div><!-- .top-links end -->

        </div>

    </div>

</div><!-- #top-bar end -->
