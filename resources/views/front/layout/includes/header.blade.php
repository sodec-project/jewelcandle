<!-- Header
    ============================================= -->
<header id="header" class="sticky-style-2">
    <div class="container clearfix">
        <div id="logo" class="divcenter">
            <a href="{{ route('home') }}" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="/storage/logo.png" alt="{{ env("APP_NAME") }}" class="divcenter"></a>
            <a href="{{ route('home') }}" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="/storage/logo.png" alt="{{ env("APP_NAME") }}" class="divcenter"></a>
        </div><!-- #logo end -->
    </div>

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->

            <!-- Primary Navigation
            ============================================= -->
            <nav id="primary-menu" class="style-2">

                <ul>
                    <li class="current">
                        <a href="{{ route('home') }}">
                            <div>Accueil</div>
                            <span>Lets Start</span>
                        </a>
                    </li>
                    <!-- Mega Menu
                    ============================================= -->
                    @foreach(\app\HelperClass\CategoryClass::listCategory() as $category)
                        @if(\app\HelperClass\CategoryClass::countSub($category->id) == 0)
                            <li>
                                <a href="{{ route('Product.Categorie.index', $category->slug) }}">
                                    <div>{{ $category->name }}</div>
                                </a>
                            </li>
                        @else
                            <li class="mega-menu">
                                <a href="#"><div>{{ $category->name }}</div></a>
                                <div class="mega-menu-content style-2 clearfix">
                                    <ul class="mega-menu-column col-md-3">
                                        <li class="mega-menu-title"><a href="#"><div>{{ $category->name }}</div></a>
                                            <ul>
                                                @foreach($category->subcategories as $sub)
                                                    <li><a href="{{ route('Product.Categorie.index', $category->slug) }}"><div>{{ $sub->name }}</div></a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                    @foreach(\app\HelperClass\ProductClass::productheaderForCategory($category->id) as $latest)
                                        <div class="mega-menu-column col-md-3">
                                            <div class="product clearfix">
                                                <div class="product-image">
                                                    <a href="{{ route('Front.Product.show', $latest->id) }}">
                                                        @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('product/'.$latest->id.'.jpg'))
                                                            <img src="/storage/product/{{ $latest->id }}.jpg" alt="{{ $latest->name }}">
                                                        @else
                                                            <img src="https://via.placeholder.com/250" alt="{{ $latest->name }}">
                                                        @endif
                                                    </a>
                                                    <!--<div class="sale-flash">50% Off*</div>-->
                                                    <div class="product-overlay">
                                                        <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Ajouter au panier</span></a>
                                                        <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Vue rapide</span></a>
                                                    </div>
                                                </div>
                                                <div class="product-desc">
                                                    <div class="product-title"><h3><a href="{{ route('Front.Product.show', $latest->id) }}">{{ $latest->name }}</a></h3></div>
                                                    <div class="product-price"><ins>{{ formatCurrency($latest->price) }}</ins></div>
                                                    {!! \app\HelperClass\ProductClass::getCountVote($latest->id) !!}
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </li><!-- .mega-menu end -->
                        @endif
                    @endforeach
                </ul>

                <!-- Top Cart
                ============================================= -->
                <div id="top-cart">
                    @if(\app\HelperClass\CartClass::initCount() == 0)
                        <a href="#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>0</span></a>
                        <div class="top-cart-content">
                            <div class="top-cart-title">
                                <h4>Mon panier</h4>
                            </div>
                            <div class="top-cart-items">
                                <span class="text-center">Aucun produit dans votre panier</span>
                            </div>
                            <div class="top-cart-action clearfix">
                                <span class="fleft top-checkout-price">0,00 €</span>
                                <a href="{{ route('Cart.index') }}" class="button button-3d button-small nomargin fright">Voir mon panier</a>
                            </div>
                        </div>
                    @else
                        <a href="#" id="top-cart-trigger"><i class="icon-shopping-cart"></i><span>{{ \app\HelperClass\CartClass::countProductToCart() }}</span></a>
                        <div class="top-cart-content">
                            <div class="top-cart-title">
                                <h4>Mon panier</h4>
                            </div>
                            <div class="top-cart-items">
                                @foreach(\app\HelperClass\CartClass::getMyCart()->cartproducts as $product)
                                    <div class="top-cart-item clearfix">
                                        <div class="top-cart-item-image">
                                            <a href="#"><img src="/storage/product/{{ $product->product_id }}.jpg" alt="{{ $product->name }}" /></a>
                                        </div>
                                        <div class="top-cart-item-desc">
                                            <a href="#">{{ $product->product->name }} - {{ $product->combination->name }}</a>
                                            <span class="top-cart-item-price">{{ formatCurrency($product->totalPrice) }}</span>
                                            <span class="top-cart-item-quantity">x {{ $product->qte }}</span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="top-cart-action clearfix">
                                <span class="fleft top-checkout-price">{{ formatCurrency(\app\HelperClass\CartClass::sumProductToCart()) }}</span>
                                <a href="{{ route('Cart.index') }}" class="button button-3d button-small nomargin fright">Voir mon panier</a>
                            </div>
                        </div>
                    @endif
                </div><!-- #top-cart end -->

                <!-- Top Search
                ============================================= -->
                <!--<div id="top-search">
                    <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                    <form action="search.html" method="get">
                        <input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
                    </form>
                </div>--><!-- #top-search end -->

            </nav><!-- #primary-menu end -->

        </div>

    </div>

</header><!-- #header end -->
