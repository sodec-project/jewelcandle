<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="SemiColonWeb" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/dark.css" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/animate.css" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="/assets/front/css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="/assets/front/css/custom.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="/assets/front/include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/assets/front/include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/assets/front/include/rs-plugin/css/navigation.css">
    @notifyCss

    @yield("style")

    <!-- Document Title
    ============================================= -->
    <title>{{ env("APP_NAME") }}</title>

    <style>

        .revo-slider-emphasis-text {
            font-size: 58px;
            font-weight: 700;
            letter-spacing: 1px;
            font-family: 'Raleway', sans-serif;
            padding: 15px 20px;
            border-top: 2px solid #FFF;
            border-bottom: 2px solid #FFF;
        }

        .revo-slider-desc-text {
            font-size: 20px;
            font-family: 'Lato', sans-serif;
            width: 650px;
            text-align: center;
            line-height: 1.5;
        }

        .revo-slider-caps-text {
            font-size: 16px;
            font-weight: 400;
            letter-spacing: 3px;
            font-family: 'Raleway', sans-serif;
        }

        .tp-video-play-button { display: none !important; }

        .tp-caption { white-space: nowrap; }

    </style>

</head>
