<!-- Footer
============================================= -->
<footer id="footer" class="dark">

    <div class="container">

        <!-- Footer Widgets
        ============================================= -->
        <div class="footer-widgets-wrap clearfix">

            <div class="col_two_third">

                <div class="col_one_third">

                    <div class="widget clearfix">

                        <img src="/storage/logo.png" alt="" class="footer-logo">

                        <div style="background: url('/assets/front/images/world-map.png') no-repeat center center; background-size: 100%;">
                            <address>
                                22 Rue Maryse Bastié<br>
                                85100 Les Sables d'Olonne<br>
                                France
                            </address>
                            <abbr title="Phone Number"><strong>Téléphone:</strong></abbr> 0899 492 648<br>
                            <abbr title="Email Address"><strong>Email:</strong></abbr> contact@jewelcandle.shop
                        </div>

                    </div>

                </div>

                <div class="col_one_third">

                    <div class="widget widget_links clearfix">

                        <h4>{{ env('APP_NAME') }}</h4>

                        <ul>
                            <li><a href="{{ route('home') }}">Acceuil</a></li>
                            <li><a href="{{ route('a_propos') }}">A Propos</a></li>
                            <li><a href="{{ route('qualite') }}">Qualité</a></li>
                            <li><a href="{{ route('contact') }}">Contactez-nous</a></li>
                            <li><a href="{{ route('reclamation') }}">Réclamation</a></li>
                            <li><a href="{{ route('mention') }}">Mentions légales</a></li>
                            <li><a href="{{ route('rgpd') }}">Protection des données</a></li>
                        </ul>

                    </div>

                </div>

                <div class="col_one_third col_last">

                    <div class="widget clearfix">
                        <h4>Derniers Produits</h4>
                        <div id="post-list-footer">
                            @foreach(\app\HelperClass\ProductClass::latestProduct() as $product)
                            <div class="spost clearfix">
                                <div class="entry-c">
                                    <div class="entry-title">
                                        <h4><a href="{{ route('Front.Product.show', $product->id) }}">{{ $product->name }}</a></h4>
                                    </div>
                                    <ul class="entry-meta">
                                        <li>{{ formatCurrency($product->price) }}</li>
                                    </ul>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>

            </div>

            <div class="col_one_third col_last">

                <div class="widget clearfix" style="margin-bottom: -20px;">

                    <div class="row">

                        <div class="col-md-6 clearfix bottommargin-sm">
                            <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
                            <a href="#"><small style="display: block; margin-top: 3px;"><strong>Like us</strong><br>on Facebook</small></a>
                        </div>
                        <div class="col-md-6 clearfix">
                            <a href="#" class="social-icon si-dark si-colored si-rss nobottommargin" style="margin-right: 10px;">
                                <i class="icon-rss"></i>
                                <i class="icon-rss"></i>
                            </a>
                            <a href="#"><small style="display: block; margin-top: 3px;"><strong>Subscribe</strong><br>to RSS Feeds</small></a>
                        </div>

                    </div>

                </div>

            </div>

        </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half">
                Copyrights &copy; 2019 All Rights Reserved by Jewelcandle.shop.<br>
            </div>

            <div class="col_half col_last tright">
                <!--<div class="fright clearfix">
                    <a href="#" class="social-icon si-small si-borderless si-facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>

                    <a href="#" class="social-icon si-small si-borderless si-twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>

                    <a href="#" class="social-icon si-small si-borderless si-gplus">
                        <i class="icon-gplus"></i>
                        <i class="icon-gplus"></i>
                    </a>

                    <a href="#" class="social-icon si-small si-borderless si-pinterest">
                        <i class="icon-pinterest"></i>
                        <i class="icon-pinterest"></i>
                    </a>

                    <a href="#" class="social-icon si-small si-borderless si-vimeo">
                        <i class="icon-vimeo"></i>
                        <i class="icon-vimeo"></i>
                    </a>

                    <a href="#" class="social-icon si-small si-borderless si-github">
                        <i class="icon-github"></i>
                        <i class="icon-github"></i>
                    </a>

                    <a href="#" class="social-icon si-small si-borderless si-yahoo">
                        <i class="icon-yahoo"></i>
                        <i class="icon-yahoo"></i>
                    </a>

                    <a href="#" class="social-icon si-small si-borderless si-linkedin">
                        <i class="icon-linkedin"></i>
                        <i class="icon-linkedin"></i>
                    </a>
                </div>-->

                <div class="clear"></div>

                <i class="icon-envelope2"></i> contact@jewelcandle.shop <span class="middot">&middot;</span> <i class="icon-headphones"></i> 0899 492 648
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->
