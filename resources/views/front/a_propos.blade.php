@extends("front.layout.app")

@section("content")
    <section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-prinz-1.jpg'); background-size: cover; background-position: center center;" data-stellar-background-ratio="0.4">

        <div class="container clearfix">
            <h1>A Propos de Jewel Candle</h1>
            <span>Découvrez un bijou en argent dans CHAQUE produit JewelCandle !</span>
            <ol class="breadcrumb">
                <li><a href="#">Acceuil</a></li>
                <li class="active">A Propos de JewelCandle</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-prinz-1.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">
                            <p>Après avoir allumé votre JewelCandle et profité de son merveilleux parfum pendant quelques heures, votre bijou va se libérer de la cire et apparaître à la surface. Emballé en sécurité dans un aluminium résistant à la chaleur, il évite ainsi tout contact direct avec la cire liquide ou la flamme.</p>
                        </div>

                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-prinz-2.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">
                            <p>	Afin de retirer le bijou de votre JewelCandle sans problème, nous vous conseillons d’attendre.<br>
                                Ainsi, brûlez votre JewelCandle jusqu’à ce que vous puissiez voir le sachet en aluminium dans son intégralité. Soyez vigilant(e) lorsque vous retirez le papier en aluminium de la bougie bijou et gardez une serviette en papier à côté de vous pour pouvoir immédiatement retirer l’excédant de cire.</p>
                        </div>

                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-prinz-3.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">
                            <p>Une fois le papier aluminium refroidi, vous pouvez commencer à retirer l’emballage. A l’intérieur de l’aluminium, vous trouverez un petit sachet contenant votre bijou. Ouvrez le sachet et découvrez le bijou que vous avez tant attendu ☺</p>
                        </div>

                    </div>

                </div>

                <div class="clearfix"></div>

                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/JC-Uberuns-prinz-5.jpg">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">
                            <p>L’attente en valait le coup ! Vous êtes maintenant propriétaire d’un magnifique bijou en argent. Chaque bijou caché à l’intérieur de nos JewelCandle possède une étiquette certifiant du matériel utilisé pour sa fabrication. Vous découvrirez la valeur de votre bijou en entrant sur notre site le code noté sur sa petite étiquette.</p>
                        </div>

                    </div>

                </div>
            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section("script")
<script type="text/javascript">
    jQuery(document).ready( function($){
        $('#shop').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                },
                price_hl: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
            var element = $(this),
                elementFilter = element.children('a').attr('data-filter'),
                elementFilterContainer = element.parents('.custom-filter').attr('data-container');

            elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

            element.append('<span>'+ elementFilterCount +'</span>');

        });

        $('.shop-sorting li').click( function() {
            $('.shop-sorting').find('li').removeClass( 'active-filter' );
            $(this).addClass( 'active-filter' );
            var sortByValue = $(this).find('a').attr('data-sort-by');
            $('#shop').isotope({ sortBy: sortByValue });
            return false;
        });
    });
</script>
@endsection
