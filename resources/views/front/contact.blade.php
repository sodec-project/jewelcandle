@extends("front.layout.app")

@section("content")
    <section id="page-title">

        <div class="container clearfix">
            <h1>Contact</h1>
            <ol class="breadcrumb">
                <li><a href="#">Accueil</a></li>
                <li class="active">Contact</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <!-- Postcontent
                ============================================= -->
                <div class="postcontent nobottommargin">

                    <h3>Contactez-nous</h3>

                    <div class="contact-widget">

                        <div class="contact-form-result"></div>

                        <form class="nobottommargin"  action="{{ route('postContact') }}" method="post">
                            @csrf

                            <div class="form-process"></div>

                            <div class="col_full">
                                <label for="template-contactform-message">Prénom <small>*</small></label>
                                <input type="text" class="sm-form-control required" name="prenom">
                            </div>
                            <div class="col_full">
                                <label for="template-contactform-message">Nom <small>*</small></label>
                                <input type="text" class="sm-form-control required" name="nom">
                            </div>
                            <div class="col_full">
                                <label for="template-contactform-message">Adresse Email <small>*</small></label>
                                <input type="email" class="sm-form-control required" name="email">
                            </div>
                            <div class="col_full">
                                <label for="template-contactform-message">Téléphone <small>*</small></label>
                                <input type="tel" class="sm-form-control required" name="telephone">
                            </div>
                            <div class="col_full">
                                <label for="template-contactform-message">Votre message <small>*</small></label>
                                <textarea class="required sm-form-control" id="template-contactform-message" name="message" rows="6" cols="30"></textarea>
                            </div>

                            <div class="col_full">
                                <button class="button button-3d nomargin" type="submit"  value="submit">Envoyer</button>
                            </div>

                        </form>
                    </div>

                </div><!-- .postcontent end -->

                <!-- Sidebar
                ============================================= -->
                <div class="sidebar col_last nobottommargin">

                    <address>
                        22 Rue Maryse Bastié<br>
                        85100 Les Sables d'Olonne <br>
                        France
                    </address>
                    <abbr title="Phone Number"><strong>Téléphone:</strong></abbr> 0899 492 648<br>
                    <abbr title="Email Address"><strong>Email:</strong></abbr> contact@jewelcandle.shop

                    <div class="widget noborder notoppadding">

                        <div class="fslider customjs testimonial twitter-scroll twitter-feed" data-username="envato" data-count="3" data-animation="slide" data-arrows="false">
                            <i class="i-plain i-small color icon-twitter nobottommargin" style="margin-right: 15px;"></i>
                            <div class="flexslider" style="width: auto;">
                                <div class="slider-wrap">
                                    <div class="slide"></div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!--<div class="widget noborder notoppadding">

                        <a href="#" class="social-icon si-small si-dark si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-dark si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-dark si-dribbble">
                            <i class="icon-dribbble"></i>
                            <i class="icon-dribbble"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-dark si-forrst">
                            <i class="icon-forrst"></i>
                            <i class="icon-forrst"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-dark si-pinterest">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-dark si-gplus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>

                    </div>-->

                </div><!-- .sidebar end -->

            </div>

        </div>

    </section><!-- #content end -->
@endsection

@section("script")
<script type="text/javascript">
    jQuery(document).ready( function($){
        $('#shop').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                },
                price_hl: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("€");

                    return parseFloat( priceNum[1] );
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
            var element = $(this),
                elementFilter = element.children('a').attr('data-filter'),
                elementFilterContainer = element.parents('.custom-filter').attr('data-container');

            elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

            element.append('<span>'+ elementFilterCount +'</span>');

        });

        $('.shop-sorting li').click( function() {
            $('.shop-sorting').find('li').removeClass( 'active-filter' );
            $(this).addClass( 'active-filter' );
            var sortByValue = $(this).find('a').attr('data-sort-by');
            $('#shop').isotope({ sortBy: sortByValue });
            return false;
        });
    });
</script>
@endsection
