@extends("front.layout.app")

@section("content")
    <section id="slider" class="slider-parallax revslider-wrap ohidden clearfix">

        <!--
        #################################
            - THEMEPUNCH BANNER -
        #################################
        -->
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-delay="10000" data-saveperformance="off" data-title="Latest Collections" style="background-color: #F6F6F6;">
                        <!-- LAYERS -->

                        <!-- LAYER NR. 2 -->
                        <img src="/storage/slide/banner-noel.jpg"
                             alt="Ocean"
                             class="rev-slidebg"
                             data-bgposition="center center"
                             data-bgfit="cover"
                             data-bgrepeat="no-repeat">

                        <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                             data-x="0"
                             data-y="110"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="700"
                             data-start="1000"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style=" color: #FFF; font-size: 30px;">Joyeux Noel & Bonne Année</div>

                        <div class="tp-caption customin ltl tp-resizeme revo-slider-caps-text uppercase"
                             data-x="0"
                             data-y="170"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="700"
                             data-start="1200"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style=" color: #FFF; font-size: 15px;">Découvrez les nouvelles bougie de noël</div>

                        <div class="tp-caption customin ltl tp-resizeme"
                             data-x="0"
                             data-y="340"
                             data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1.3;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                             data-speed="700"
                             data-start="1550"
                             data-easing="easeOutQuad"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power4.easeIn" style=""><a href="#" class="button button-border button-large button-rounded tright nomargin"><span>Découvrir</span> <i class="icon-angle-right"></i></a></div>

                    </li>
                    <!-- SLIDE  -->
                </ul>
            </div>
        </div>

        <!-- END REVOLUTION SLIDER -->

    </section>

    <!-- Content
    ============================================= -->
    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="col-md-2">&nbsp;</div>
                <div class="col-md-8 nopadding">

                    <div class="col-md-4 noleftpadding bottommargin-sm ">
                        <a href="/categorie/edition-classique"><img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/11/classic-edition-bougie-parfumée-avec-bijou-jewelcandle-smallbox-desktop.jpg" alt="Image"></a>
                    </div>

                    <div class="col-md-4 noleftpadding bottommargin-sm">
                        <a href="/categorie/produit-de-bain"><img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/12/boule-de-bain-avec-bijou-jewelcandle-small-box-desktop.jpg" alt="Image"></a>
                    </div>

                    <div class="col-md-4 noleftpadding">
                        <a href="/categorie/edition-speciales"><img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/12/owl-i-need-is-you-bougie-parfumée-avec-bijou-jewelcandle-small-box-desktop-1.jpg" alt="Image"></a>
                    </div>

                    <div class="clear"></div>


                </div>
                <div class="col-md-2">&nbsp;</div>

                <!--<div class="col-md-4 nopadding">
                    <a href="#"><img src="https://via.placeholder.com/250" alt="Image"></a>
                </div>-->

                <div class="clear"></div>

                <div class="section notopborder nomargin">

                    <div class="container clearfix">

                        <div class="col_two_fifth topmargin-sm nobottommargin" style="min-height: 350px">
                            <img src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/1-2-3-Lilac.gif" alt="">
                        </div>

                        <div class="col_three_fifth nobottommargin col_last">

                            <div class="heading-block topmargin-sm">
                                <h2>Comment ça marche ?</h2>
                                <span>Découvrez le concept JewelCandle en 3 étapes simplissimes !</span>
                            </div>

                            <ol>
                                <li>Allumez votre JewelCandle, mettez-vous à l'aise et appréciez son parfum merveilleux.</li>
                                <li>Après quelques heures d'attente et de combustion, découvrez un magnifique bijou en argent soigneusement emballé dans la cire.</li>
                                <li>Découvrez la valeur de votre bijou en cliquant ICI !</li>
                            </ol>

                            <a href="#myModal1" data-lightbox="inline" class="button button-3d button-large">Voir la vidéo</a>

                            <div class="modal1 mfp-hide" id="myModal1">
                                <div class="block divcenter" style="background-color: #FFF; max-width: 1024px;">
                                    <div class="center clearfix" style="padding: 50px;">
                                        <iframe src="https://www.jewel-candle.com/fr/wp-content/uploads/2019/02/How-To-Website-NEW.mp4?id=0" width="900" height="430" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="clear"></div>

                <div class="tabs topmargin-lg clearfix" id="tab-3">

                    <ul class="tab-nav clearfix">
                        <li><a href="#tabs-9">Nouveauté</a></li>
                        <li><a href="#tabs-10">Meilleurs Ventes</a></li>
                    </ul>

                    <div class="tab-container">

                        <div class="tab-content clearfix" id="tabs-9">

                            <div id="shop" class="shop clearfix">

                                @foreach($arrivals as $arrival)
                                    <div class="product clearfix">
                                        <div class="product-image">
                                            <a href="{{ route('Front.Product.show', $arrival->id) }}">
                                                @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('product/'.$arrival->id.'.jpg'))
                                                    <img src="/storage/product/{{ $arrival->id }}.jpg" alt="{{ $arrival->name }}">
                                                @else
                                                    <img src="https://via.placeholder.com/250" alt="{{ $arrival->name }}">
                                                @endif
                                            </a>
                                            <!--<div class="sale-flash">50% Off*</div>-->
                                            <div class="product-overlay">
                                                <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Ajouter au panier</span></a>
                                                <a id="ajax-product-view" data-id="{{ $arrival->id }}" href="/product/{{ $arrival->id }}/ajaxview" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Vue rapide</span></a>
                                            </div>
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-title"><h3><a href="{{ route('Front.Product.show', $arrival->id) }}">{{ $arrival->name }}</a></h3></div>
                                            <div class="product-price"><ins>{{ formatCurrency($arrival->price) }}</ins></div>
                                            {!! \app\HelperClass\ProductClass::getCountVote($arrival->id) !!}
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                        </div>

                        <div class="tab-content clearfix" id="tabs-10">

                            <div id="shop" class="shop clearfix">

                                @foreach($halls as $hall)
                                    <div class="product clearfix">
                                        <div class="product-image">
                                            <a href="{{ route('Front.Product.show', $hall->id) }}">
                                                @if(\Illuminate\Support\Facades\Storage::disk('public')->exists('product/'.$hall->product->id.'.jpg'))
                                                    <img src="/storage/product/{{ $hall->product->id }}.jpg" alt="{{ $hall->product->name }}">
                                                @else
                                                    <img src="https://via.placeholder.com/250" alt="{{ $hall->product->name }}">
                                                @endif
                                            </a>
                                            <!--<div class="sale-flash">50% Off*</div>-->
                                            <div class="product-overlay">
                                                <a href="#" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Ajouter au panier</span></a>
                                                <a href="include/ajax/shop-item.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Vue rapide</span></a>
                                            </div>
                                        </div>
                                        <div class="product-desc">
                                            <div class="product-title"><h3><a href="{{ route('Front.Product.show', $hall->id) }}">{{ $hall->product->name }}</a></h3></div>
                                            <div class="product-price"><ins>{{ formatCurrency($hall->product->price) }}</ins></div>
                                            {!! \app\HelperClass\ProductClass::getCountVote($hall->product->id) !!}
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>

                    </div>

                </div>

                <div class="clear bottommargin-sm"></div>


                <div class="col_two_third subscribe-widget">
                    <div class="fancy-title title-border">
                        <h4>Newsletter</h4>
                    </div>
                    <p>Abonnez-vous à notre newsletter pour recevoir des informations importantes et des offres exceptionnelles:</p>
                    <div class="widget-subscribe-form-result"></div>
                    <form id="widget-subscribe-form2" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                        <div class="input-group divcenter">
                            <span class="input-group-addon"><i class="icon-email2"></i></span>
                            <input type="email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Entrez votre adresse mail">
                            <span class="input-group-btn">
									<button class="btn btn-default" type="submit">Souscrire</button>
								</span>
                        </div>
                    </form>
                </div>

                <div class="col_one_third col_last">
                    <div class="fancy-title title-border">
                        <h4>Restez connecter avec nous</h4>
                    </div>

                    <a href="#" class="social-icon si-facebook" data-toggle="tooltip" data-placement="top" title="Facebook">
                        <i class="icon-facebook"></i>
                        <i class="icon-facebook"></i>
                    </a>

                    <a href="#" class="social-icon si-twitter" data-toggle="tooltip" data-placement="top" title="Twitter">
                        <i class="icon-twitter"></i>
                        <i class="icon-twitter"></i>
                    </a>
                </div>

                <div class="clear"></div>

            </div>

            <div class="section nobottommargin">
                <div class="container clearfix">

                    <div class="col_one_fourth nobottommargin">
                        <div class="feature-box fbox-plain fbox-dark fbox-small">
                            <div class="fbox-icon">
                                <i class="icon-thumbs-up2"></i>
                            </div>
                            <h3>100% Original</h3>
                            <p class="notopmargin">Les produits présenté sur ce site sont conforme à l'original</p>
                        </div>
                    </div>

                    <div class="col_one_fourth nobottommargin">
                        <div class="feature-box fbox-plain fbox-dark fbox-small">
                            <div class="fbox-icon">
                                <i class="icon-credit-cards"></i>
                            </div>
                            <h3>Mode de Paiement</h3>
                            <p class="notopmargin">Nous acceptons le paiement par carte bleu (Visa, Mastercard, American Express)</p>
                        </div>
                    </div>

                    <div class="col_one_fourth nobottommargin">
                        <div class="feature-box fbox-plain fbox-dark fbox-small">
                            <div class="fbox-icon">
                                <i class="icon-truck2"></i>
                            </div>
                            <h3>Livraison Gratuite</h3>
                            <p class="notopmargin">Nous pouvons vous livrez gratuitement en france à partir de 190€ d'achat</p>
                        </div>
                    </div>

                    <div class="col_one_fourth nobottommargin col_last">
                        <div class="feature-box fbox-plain fbox-dark fbox-small">
                            <div class="fbox-icon">
                                <i class="icon-undo"></i>
                            </div>
                            <h3>Retours sous 15 jours</h3>
                            <p class="notopmargin">Retournez ou échangez les articles achetés dans les 15 jours.</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </section><!-- #content end -->
@endsection
<?php
header("Access-Control-Allow-Origin: *");              // Tous les domaines
?>
@section("script")
    <script type="text/javascript">

        var tpj=jQuery;
        tpj.noConflict();

        tpj(document).ready(function() {

            var apiRevoSlider = tpj('.tp-banner').show().revolution(
                {
                    sliderType:"standard",
                    jsFileLocation:"include/rs-plugin/js/",
                    sliderLayout:"fullwidth",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {},
                    responsiveLevels:[1200,992,768,480,320],
                    gridwidth:1140,
                    gridheight:500,
                    lazyType:"none",
                    shadow:0,
                    spinner:"off",
                    autoHeight:"off",
                    disableProgressBar:"on",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        disableFocusListener:false,
                    },
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        onHoverStop:"off",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        },
                        arrows: {
                            style: "ares",
                            enable: true,
                            hide_onmobile: false,
                            hide_onleave: false,
                            tmp: `<div class="tp-title-wrap">	<span class="tp-arr-titleholder"></span> </div>`,
                            left: {
                                h_align: "left",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            },
                            right: {
                                h_align: "right",
                                v_align: "center",
                                h_offset: 10,
                                v_offset: 0
                            }
                        }
                    }
                });

            apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
                SEMICOLON.slider.sliderParallaxDimensions();
            });

        }); //ready

    </script>
    <script type="text/javascript">

    </script>
@endsection
