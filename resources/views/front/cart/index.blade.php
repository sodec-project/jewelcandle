@extends("front.layout.app")

@section("content")
    <section id="page-title">

        <div class="container clearfix">
            <h1>Mon Panier</h1>
            <ol class="breadcrumb">
                <li><a href="#">Accueil</a></li>
                <li class="active">Mon Panier</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->

    <section id="content">

        <div class="content-wrap">

            <div class="container clearfix">

                <div class="table-responsive bottommargin">

                    <form id="formCart" action="/api/cart/product" method="POST">
                        @method("PUT")
                        <table class="table cart" id="panier">
                            <thead>
                            <tr>
                                <th class="cart-product-remove">&nbsp;</th>
                                <th class="cart-product-thumbnail">&nbsp;</th>
                                <th class="cart-product-name">Produit</th>
                                <th class="cart-product-price">Prix Unitaire</th>
                                <th class="cart-product-quantity">Quantité</th>
                                <th class="cart-product-subtotal">Prix Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cart->cartproducts as $product)
                                <tr class="cart_item">
                                    <td class="cart-product-remove">
                                        <a id="btnTrashProduct" href="/api/cart/product/{{ $product->id }}"
                                           class="remove" title="Remove this item"><i class="icon-trash2"></i></a>
                                    </td>

                                    <td class="cart-product-thumbnail">
                                        <a href="{{ route('Front.Product.show', $product->product_id) }}"><img
                                                width="64" height="64"
                                                src="/storage/product/{{ $product->product_id }}.jpg"
                                                alt="{{ $product->product->name }}"></a>
                                    </td>

                                    <td class="cart-product-name">
                                        <a href="#">{{ $product->product->name }}
                                            - {!! \app\HelperClass\ProductClass::iconCombination($product->combination->name) !!} {{ $product->combination->name }}</a>
                                    </td>

                                    <td class="cart-product-price">
                                        <span class="amount">{{ formatCurrency($product->unitPrice) }}</span>
                                    </td>

                                    <td class="cart-product-quantity">
                                        <div class="quantity clearfix">
                                            <input class="product_id" type="hidden" name="product_id" value="{{ $product->id }}">
                                            <input type="text" name="quantity" value="{{ $product->qte }}"
                                                   class="qty product_qte" disabled/>
                                        </div>
                                    </td>

                                    <td class="cart-product-subtotal">
                                        <span class="amount">{{ formatCurrency($product->totalPrice) }}</span>
                                    </td>
                                </tr>
                            @endforeach

                            <tr class="cart_item">
                                <td colspan="6">
                                    <div class="row clearfix">
                                        <div class="col-md-4 col-xs-4 nopadding">
                                            <div class="col-md-8 col-xs-7 nopadding">
                                                <input type="text" value="" class="sm-form-control"
                                                       placeholder="Entrez le code de promotion..."/>
                                            </div>
                                            <div class="col-md-4 col-xs-5">
                                                <a href="#" class="button button-3d button-black nomargin">Appliquer le
                                                    code</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-xs-8 nopadding">
                                            <a href="{{ route('Checkout.index') }}" class="button button-3d notopmargin fright">Passer
                                                commande</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>

                        </table>
                    </form>

                </div>

                <div class="row clearfix">
                    <div class="col-md-6 clearfix">
                        &nbsp;
                    </div>

                    <div class="col-md-6 clearfix">
                        <div class="table-responsive">
                            <h4>Total du panier</h4>

                            <table class="table cart">
                                <tbody>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Sous Total</strong>
                                    </td>

                                    <td class="cart-product-name">
                                        <span id="firstTotal" class="amount">{{ formatCurrency($cart->total) }}</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Frais de livraison (estimation)</strong>
                                    </td>

                                    <td class="cart-product-name">
                                        <span class="amount">5,60 €</span>
                                    </td>
                                </tr>
                                <tr class="cart_item">
                                    <td class="cart-product-name">
                                        <strong>Total</strong>
                                    </td>

                                    <td class="cart-product-name">
                                        <span id="fulltotal"
                                              class="amount color lead"><strong> {{ formatCurrency($cart->total + 5.60) }}</strong></span>
                                    </td>
                                </tr>
                                </tbody>

                            </table>

                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section><!-- #content end -->

@endsection

@section("script")
    <script src="/assets/front/js/pages/cart/index.js" type="text/javascript"></script>
@endsection
