@extends("front.layout.app")

@section("content")
    <section id="page-title">

        <div class="container clearfix">
            <h1>Réclamations</h1>
            <ol class="breadcrumb">
                <li><a href="#">Accueil</a></li>
                <li class="active">Réclamations</li>
            </ol>
        </div>

    </section><!-- #page-title end -->

    <!-- Content
		============================================= -->
    <section id="content">
        <div class="content-wrap">
            <div class="section notopborder nomargin">

                <div class="container clearfix">

                    <div class="col_full">
                        <strong>Droit de rétractation du consommateur</strong>
                        <p>Vous avez le droit d’annuler ce contrat dans les quatorze jours sans avoir à justifier de motifs. Le délai de rétractation est de quatorze jours à compter de la date “à laquelle vous ou un tiers désigné par vous-même, qui n’est pas le transporteur, a pris possession physique des marchandises”. Pour exercer votre droit de rétractation, vous devez nous informer par l’intermédiaire d’une déclaration claire (par exemple, en nous envoyant une lettre par courrier postal ou un e-mail) de votre décision d’annuler ce contrat à cette adresse:</p>
                        <p>
                            <strong>
                                Jewel Candle Shop<br>
                                Réclamation<br>
                                22 Rue Maryse Bastié<br>
                                85100 Les Sables d'Olonne<br>
                                support@jewelcandleshop.freshdesk.com
                            </strong>
                        </p>
                        <p>Afin de respecter le délai de rétractation, il vous suffit de nous renvoyer votre déclaration d’exercice du droit de rétractation avant l’expiration du délai de rétractation.</p>
                        <strong>Conséquences de la révocation</strong>
                        <p>Si vous annulez ce contrat, nous vous remboursons la totalité des sommes versées, hors frais de livraison. Le remboursement sera effectué immédiatement ou au plus tard dans les quatorze jours à compter de la date à laquelle nous avons reçu la notification de votre annulation du contrat. Pour ce remboursement, nous utilisons le même moyen de paiement que celui utilisé pour l’achat initial, sauf en cas d’accords explicitement convenus préalablement; dans tous les cas, aucun frais ne vous sera facturé pour ce remboursement.</p>
                        <p>Toutefois le remboursement peut être différé jusqu’à récupération des biens ou jusqu’à ce que vous ayez fourni une preuve de l’expédition de la marchandise, la date retenue étant la plus rapprochée.</p>
                        <p>Vous devez retourner les biens immédiatement et en état au plus tard dans les quatorze jours à compter de la date à laquelle vous nous informez de l’annulation de ce contrat. Le délai est respecté seulement si vous renvoyez les biens  avant l’échéance des quatorze jours.</p>
                        <p><i>Vous assumez les coûts directs du renvoi des marchandises. Vous assumez le coût de dépréciation des biens et vous devez payer la perte de valeur des biens, si après que nous les recevons, nous pouvons constater qu’ils ne sont pas dans l’état dans lequel ils vous ont été envoyés.</i></p>
                    </div>

                </div>

            </div>
        </div>
    </section>
@endsection

@section("script")

@endsection
