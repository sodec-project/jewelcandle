<?php

namespace Tests\Feature;

use App\Model\Product\ProductCategorie;
use app\Packages\Hiboutik\Categories;
use App\Repository\Product\ProductCategoryRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    use RefreshDatabase;


    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpdateCategory()
    {
        $boutique = new Categories();

        foreach ($boutique->listCategories() as $listCategory) {
            $categorieIsExist = ProductCategorie::query()->where('name', 'LIKE', '%'.$listCategory['category_name'])->count();
            if($categorieIsExist == 0) {
                ProductCategorie::query()->create([
                    "name"  => $listCategory['category_name']
                ]);
            } else {
                $categorie = ProductCategorie::query()->where('name', 'LIKE', '%'.$listCategory['category_name'])->first();
                ProductCategorie::query()->find($categorie->id)->update([
                    "name"  => $listCategory['category_name']
                ]);
            }
        }
        $categorie = ProductCategorie::query()->where('name', 'LIKE', '%'.$listCategory['category_name'])->get();

        $this->assertCount(1, $categorie);
    }
}
