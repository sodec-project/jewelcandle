<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["namespace" => "Front"], function () {
    Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);
    Route::get('a-propos', ['as' => "a_propos", "uses" => "HomeController@a_propos"]);
    Route::get('qualite', ['as' => "qualite", "uses" => "HomeController@qualite"]);
    Route::get('contact', ['as' => "contact", "uses" => "HomeController@contact"]);
    Route::post('contact', ['as' => "postContact", "uses" => "HomeController@postContact"]);
    Route::get('reclamation', ['as' => "reclamation", "uses" => "HomeController@reclamation"]);
    Route::get('mention', ['as' => "mention", "uses" => "HomeController@mention"]);
    Route::get('rgpd', ['as' => "rgpd", "uses" => "HomeController@rgpd"]);

    Route::group(["prefix" => "product", "namespace" => "Product"], function (){
        Route::get('/', ["as" => "Front.products", "uses" => "ProductController@index"]);
        Route::get('{id}', ["as" => "Front.Product.show", "uses" => "ProductController@show"]);
        Route::get('{id}/ajaxview', 'ProductController@ajaxview');


    });

    Route::group(["prefix" => "categorie", "namespace" => "Categories"], function (){
        Route::get('/{slug_categorie}', ["as" => "Product.Categorie.index", "uses" => "CategoryController@index"]);
    });

    Route::group(["prefix" => "cart", "namespace" => "Cart"], function (){
        Route::get('/', ["as" => "Cart.index", "uses" => "CartController@index"]);
        Route::post('add', ["as" => "Cart.addProduct", "uses" => "CartController@addProduct"]);
    });

    Route::group(["prefix" => "checkout", "namespace" => "Checkout"], function (){
        Route::get('/', ["as" => "Checkout.index", "uses" => "CheckoutController@index"]);
        Route::get('/shipping', ["as" => "Checkout.shipping", "uses" => "CheckoutController@shipping"]);
    });

    Route::get('valeur', ["as" => "Front.valeur", "uses" => "HomeController@valeur"]);
    Route::post('valeur', ["as" => "Front.postValeur", "uses" => "HomeController@postValeur"]);
});
Route::group(["prefix" => "back240792", "namespace" => "Back"], function () {
    Route::get('/', ["as" => "Back.home", "uses" => "HomeController@index"]);

    Route::group(["prefix" => "catalog", "namespace" => "Catalog"], function (){

        Route::group(["prefix" => "category", "namespace" => "Category"], function (){
            Route::get('/', ["as" => "Catalog.Category.index", "uses" => "CategoryController@index"]);
        });

        Route::group(["prefix" => "product", "namespace" => "Product"], function () {
            Route::get('/', ["as" => "Catalog.Product.index", "uses" => "ProductController@index"]);
        });
    });
});

Auth::routes();
Route::get('logout', ["as" => "logout", "uses" => "Auth\LoginController@logout"]);
Route::get('test', 'TestController@test');
