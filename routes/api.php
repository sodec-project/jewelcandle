<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["prefix" => "cart", "namespace" => "Api\Cart"], function (){
    Route::get('/', 'CartController@index');
    Route::get('/getTotal', 'CartController@getTotal');
    Route::get('/firstTotal', 'CartController@firstTotal');

    Route::group(["prefix" => "product"], function (){
        Route::put('/', 'CartProductController@updateCart');
        Route::delete('{product_id}', 'CartProductController@delete');
    });
});
