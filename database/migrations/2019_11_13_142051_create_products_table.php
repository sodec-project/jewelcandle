<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_categorie_id');
            $table->bigInteger('product_sub_categorie_id');
            $table->string('name');
            $table->string('reference');
            $table->string('price');
            $table->text('summary')->nullable();
            $table->longText('description')->nullable();
            $table->integer('combination')->default(0)->comment('Definit si oui ou non ont utilise des attribues');
            $table->integer('quantity')->nullable();
            $table->string('weight');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
