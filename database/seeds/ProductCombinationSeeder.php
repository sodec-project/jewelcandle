<?php

use Illuminate\Database\Seeder;

class ProductCombinationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param \App\Repository\Product\ProductRepository $productRepository
     * @return void
     */
    public function run(\App\Repository\Product\ProductRepository $productRepository)
    {
        $lists = $productRepository->list();

        foreach ($lists as $list) {
            \App\Model\Product\ProductCombination::query()->create([
                "product_id"    => $list->id,
                "name"          => "Bague S",
                "price"         => 39,
                "quantity"      => 10
            ]);

            \App\Model\Product\ProductCombination::query()->create([
                "product_id"    => $list->id,
                "name"          => "Bague M",
                "price"         => 39,
                "quantity"      => 10
            ]);

            \App\Model\Product\ProductCombination::query()->create([
                "product_id"    => $list->id,
                "name"          => "Bague L",
                "price"         => 39,
                "quantity"      => 10
            ]);

            \App\Model\Product\ProductCombination::query()->create([
                "product_id"    => $list->id,
                "name"          => "Collier",
                "price"         => 39,
                "quantity"      => 10
            ]);

            \App\Model\Product\ProductCombination::query()->create([
                "product_id"    => $list->id,
                "name"          => "Charm",
                "price"         => 29,
                "quantity"      => 10
            ]);

            \App\Model\Product\ProductCombination::query()->create([
                "product_id"    => $list->id,
                "name"          => "Boucle d'oreille",
                "price"         => 29,
                "quantity"      => 10
            ]);

            \App\Model\Product\ProductCombination::query()->create([
                "product_id"    => $list->id,
                "name"          => "Bracelet",
                "price"         => 39,
                "quantity"      => 10
            ]);
        }
    }
}
