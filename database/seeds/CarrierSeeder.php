<?php

use Illuminate\Database\Seeder;

class CarrierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // TODO: Colissimo Seeder

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 0 - 0.25Kg",
            "min_weight" => "0.000",
            "max_weight" => "0.250",
            "price" => "5.60",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 0 - 0.25Kg signature",
            "min_weight" => "0.000",
            "max_weight" => "0.250",
            "price" => "6.51",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo 0 - 0.25Kg Relais",
            "min_weight" => "0.000",
            "max_weight" => "0.250",
            "price" => "4.60",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 0.25 - 0.50Kg",
            "min_weight" => "0.250",
            "max_weight" => "0.500",
            "price" => "6.30",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 0.25 - 0.50kg signature",
            "min_weight" => "0.250",
            "max_weight" => "0.500",
            "price" => "7.27",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo 0.25 - 0.50Kg Relais",
            "min_weight" => "0.250",
            "max_weight" => "0.500",
            "price" => "5.30",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 0.50 - 1Kg",
            "min_weight" => "0.500",
            "max_weight" => "1",
            "price" => "7.67",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 0.50 - 1kg signature",
            "min_weight" => "0.500",
            "max_weight" => "1",
            "price" => "8.62",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo 0.50 - 1Kg Relais",
            "min_weight" => "0.500",
            "max_weight" => "1",
            "price" => "6.70",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 1 - 2Kg",
            "min_weight" => "1",
            "max_weight" => "2",
            "price" => "8.50",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo Home 1 - 2kg signature",
            "min_weight" => "1",
            "max_weight" => "2",
            "price" => "9.45",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Colissimo 1 - 2Kg Relais",
            "min_weight" => "1",
            "max_weight" => "2",
            "price" => "7.55",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 1
        ]);

        // TODO: CHRONOPOST SEEDER

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Chrono Avant 10H 0 - 2Kg",
            "min_weight" => "0",
            "max_weight" => "2",
            "price" => "20.52",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 2
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Chrono Avant 13H 0 - 2Kg",
            "min_weight" => "0",
            "max_weight" => "2",
            "price" => "16.60",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 2
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Chrono Avant 18H 0 - 2Kg",
            "min_weight" => "0",
            "max_weight" => "2",
            "price" => "13.00",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 2
        ]);

        // TODO: Mondial Relai

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Mondial Relai 0 - 0.5Kg",
            "min_weight" => "0",
            "max_weight" => "0.500",
            "price" => "3.79",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 3
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Mondial Relai 0.5 - 1Kg",
            "min_weight" => "0.500",
            "max_weight" => "1",
            "price" => "4.37",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 3
        ]);

        \App\Model\Improve\Carrier\Carrier::query()->create([
            "name"  => "Mondial Relai 1 - 2Kg",
            "min_weight" => "1",
            "max_weight" => "2",
            "price" => "4.96",
            "sendcloud_id" => 0,
            "trackingUrl" => "",
            "type" => 3
        ]);

    }
}
