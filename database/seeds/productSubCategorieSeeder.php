<?php

use Illuminate\Database\Seeder;

class productSubCategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Product\ProductSubCategory::query()->create([
            "product_categorie_id"  => 1,
            "name"                  => "Edition Classique",
            "slug"                  => \Illuminate\Support\Str::slug('Edition Classique')
        ]);

        \App\Model\Product\ProductSubCategory::query()->create([
            "product_categorie_id"  => 2,
            "name"                  => "Les Inédites",
            "slug"                  => \Illuminate\Support\Str::slug('Les Inédites')
        ]);

        \App\Model\Product\ProductSubCategory::query()->create([
            "product_categorie_id"  => 2,
            "name"                  => "Collections Capsules",
            "slug"                  => \Illuminate\Support\Str::slug('Collections Capsules')
        ]);

        \App\Model\Product\ProductSubCategory::query()->create([
            "product_categorie_id"  => 2,
            "name"                  => "Bougies sans bijoux",
            "slug"                  => \Illuminate\Support\Str::slug('Bougies sans bijoux')
        ]);

        \App\Model\Product\ProductSubCategory::query()->create([
            "product_categorie_id"  => 2,
            "name"                  => "SweetHeart",
            "slug"                  => \Illuminate\Support\Str::slug('SweetHeart')
        ]);
        \App\Model\Product\ProductSubCategory::query()->create([
            "product_categorie_id"  => 3,
            "name"                  => "Produit de Bain",
            "slug"                  => \Illuminate\Support\Str::slug('Produit de Bain')
        ]);

        \App\Model\Product\ProductSubCategory::query()->create([
            "product_categorie_id"  => 4,
            "name"                  => "Accessoires",
            "slug"                  => \Illuminate\Support\Str::slug('Accessoires')
        ]);
    }
}
