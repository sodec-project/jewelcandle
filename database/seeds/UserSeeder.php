<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::query()->create([
            "name"  => "Administrateur",
            "email" => "contact@jewelcandle.shop",
            "password" => bcrypt('1992_Maxime')
        ]);
    }
}
