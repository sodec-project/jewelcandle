<?php

use Illuminate\Database\Seeder;

class productCategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Product\ProductCategorie::query()->create([
            "name"  => "Edition Classique",
            "slug"  => \Illuminate\Support\Str::slug("Edition Classique")
        ]);

        \App\Model\Product\ProductCategorie::query()->create([
            "name"  => "Edition Spéciale",
            "slug"  => \Illuminate\Support\Str::slug("Edition Spéciale")
        ]);

        \App\Model\Product\ProductCategorie::query()->create([
            "name"  => "Produit de Bain",
            "slug"  => \Illuminate\Support\Str::slug("Produit de Bain")
        ]);

        \App\Model\Product\ProductCategorie::query()->create([
            "name"  => "Accessoires",
            "slug"  => \Illuminate\Support\Str::slug("Accessoires")
        ]);
    }
}
