<?php

use Illuminate\Database\Seeder;

class productSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Product\Product::query()->create([
            "product_categorie_id"      => 1,
            "product_sub_categorie_id"  => 1,
            "name"                      => "My Favorite Person",
            "reference"                 => "ECMFP001",
            "price"                     => "39.00",
            "summary"                   => "Un amour de bougie rouge à la fraise",
            "description"               => "Dans la vie, il est très important de dire à nos proches combien nous les aimons. C’est ce principe qui nous a inspiré cette bougie rouge à la fraise, véritable concentré d’amour et de tendresse. Laissez donc la JewelCandle My Favorite Person exprimer tout votre amour à une personne qui vous est chère. Son délicat parfum lui rappellera votre forte relation, et elle découvrira par la suite un joli bijou en argent qu’elle pourra porter sur elle pour vous avoir toujours un peu à ses côtés. En somme, une jolie attention qui ira droit au cœur de la chanceuse destinataire de cette bougie.",
            "combination"               => 1,
            "quantity"                  => 8,
            "slug"                      => \Illuminate\Support\Str::slug("My Favorite Person")
        ]);

        \App\Model\Product\Product::query()->create([
            "product_categorie_id"      => 1,
            "product_sub_categorie_id"  => 1,
            "name"                      => "Cookies & Cream",
            "reference"                 => "ECCC001",
            "price"                     => "39.00",
            "summary"                   => "La bougie parfumée Cookies : la crème de la crème",
            "description"               => "Dans la vie, il est très important de dire à nos proches combien nous les aimons. C’est ce principe qui nous a inspiré cette bougie rouge à la fraise, véritable concentré d’amour et de tendresse. Laissez donc la JewelCandle My Favorite Person exprimer tout votre amour à une personne qui vous est chère. Son délicat parfum lui rappellera votre forte relation, et elle découvrira par la suite un joli bijou en argent qu’elle pourra porter sur elle pour vous avoir toujours un peu à ses côtés. En somme, une jolie attention qui ira droit au cœur de la chanceuse destinataire de cette bougie.",
            "combination"               => 1,
            "quantity"                  => 8,
            "slug"                      => \Illuminate\Support\Str::slug("Cookies & Cream")
        ]);
    }
}
