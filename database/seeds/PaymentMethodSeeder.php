<?php

use Illuminate\Database\Seeder;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Improve\PaymentMethod::query()->create(["name" => "Virement Bancaire"]);
        \App\Model\Improve\PaymentMethod::query()->create(["name" => "Carte Bancaire"]);
    }
}
