<?php

return [
    /*
     * ----------------------------------------------------
     * Sendinblue Credentials
     * ----------------------------------------------------
     *
     * This option specifies the Sendinblue credentials for
     * your account. You can put it here but I strongly
     * recommend to put thoses settings into your
     * .env & .env.example file.
     *
     */
    'apikey' => env('SENDINBLUE_APIKEY', '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'),
    'partnerkey' => env('SENDINBLUE_PARTNERKEY', null),
];
